﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Quotations.Web")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ITGroup Technology Consultants, Inc.")]
[assembly: AssemblyProduct("Quotations.Web")]
[assembly: AssemblyCopyright("Copyright © - 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3d5900ae-111a-45be-96b3-d9e4606ca793")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.8")]
[assembly: AssemblyFileVersion("1.0.8")]

/*
*************************************************************************************************************************************************************************************************
Versión 1.0.8 - AJCO on 2018-11-20
- Corrección a proceso de selección de productos para la cotización para evitar que se mezclen las cantidades con las entradas por otros usuarios.
*************************************************************************************************************************************************************************************************
Versión 1.0.7 - AJCO on 2018-11-02
- Corrección para agrandar campo de ciudad en mantenimiento de clientes para permitir más caracteres.
- Ajuste a la lista de ItemSearch para que el DataAccessMode sea Client, y así se pueda filtrar por usuario.
- Se incluyó manejo para evitar que dos usuarios puedan editar la misma cotización al mismo tiempo (OptimisticLock).
*************************************************************************************************************************************************************************************************
Versión 1.0.6 - AJCO on 2018-10-19
- Corrección para que el botón de Print Quotation esté disponible siempre para los usuarios con el rol de Administrators.
- Corrección para que sólo los usuarios con el rol Administrators o Edit BTC Number for Quotations puedan editar el proyecto de una cotización
  que ya estaba asignada a un proyecto previo.
*************************************************************************************************************************************************************************************************
Versión 1.0.5 - AJCO on 2018-09-24
- Corrección para que los Other Charges no se consideren en el cálculo de la comisión.
*************************************************************************************************************************************************************************************************
Versión 1.0.4 - AJCO on 2018-09-20
- Corrección en pantalla de anadir productos a la cotización, manejo de la lista de productos.
*************************************************************************************************************************************************************************************************
Versión 1.0.3 - AJCO on 2018-09-14
- Corrección en mantenimiento de productos para que los precios se recalculen correctamente si el costo cambia, y de igual forma si el porciento es cambiado.
- Correcciones en proceso de entrada de cotización (otros costos) para que el tax y la comisión se refresquen si se aplica algún descuento o cargo adicional.
- Ajustes para mejorar performance en la carga de la pantalla de búsqueda de productos.
*************************************************************************************************************************************************************************************************
Versión 1.0.2 - AJCO on 2018-09-05
- Corrección al renglón de Labor para que se calcule correctamente de acuerdo a la cantidad de productos anadidos a la cotización.
- Ajustes a pantalla de otros costos para incluir renglón del total de la cotización.
- Ajustes a reportes de Cotización (con costo y labor) para mostrar renglón de IVU.
*************************************************************************************************************************************************************************************************
Versión 1.0.1 - AJCO on 2018-08-20
- Corrección para el cálculo del ExtPrice de la cotización en caso que los precios de los productos hayan cambiado. El campo se cambió a uno calculado, pero que se persiste en la base de datos.
- Se incluyó botón para refrescar los precios de los productos. El botón sólo será visible a los usuarios que pueden hacer cambios a la cotización.
*************************************************************************************************************************************************************************************************
 */
