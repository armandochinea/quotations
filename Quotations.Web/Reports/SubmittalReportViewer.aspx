﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmittalReportViewer.aspx.cs" Inherits="Quotations.Web.Reports.SubmittalReportViewer" %>

<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" Text="Print">
            <ClientSideEvents CheckedChanged="function(s, e) {

}" Click="function(s, e) {
    // The following line prints the document.&nbsp;
    // To print a specific document page, specify its index as the method's parameter.&nbsp;
    DocumentViewer.Print();

    // The following line saves the document as a PDF file.&nbsp;
    // To export the document to a specific format, specify it as the method's parameter.&nbsp;
    // The following formats are currently supported: &nbsp;
    // 'csv', 'html', 'image', 'mht', 'pdf', 'rtf', 'txt', 'xls', and 'xlsx'.&nbsp;
    DocumentViewer.ExportTo('pdf');
}" />
        </dx:ASPxButton>
        <dx:ASPxWebDocumentViewer runat="server" ClientInstanceName="SubmittalReportDocumentViewer" ReportSourceId="Quotations.Web.Reports.SubmittalReport">
            <ClientSideEvents Init="function(s, e) {
	DevExpress.Report.Preview.AsyncExportApproach=true&nbsp;
}" />
        </dx:ASPxWebDocumentViewer>
        <div>
        </div>
    </form>
</body>
</html>
