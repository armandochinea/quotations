﻿namespace Quotations.Web.Reports
{
	partial class QuotationCostLaborReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.DataAccess.Sql.StoredProcQuery storedProcQuery1 = new DevExpress.DataAccess.Sql.StoredProcQuery();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter1 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter2 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.CustomSqlQuery customSqlQuery1 = new DevExpress.DataAccess.Sql.CustomSqlQuery();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuotationCostLaborReport));
			DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
			this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.dataSource = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
			this.groupHeaderBand1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.groupHeaderBand2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
			this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
			this.groupFooterBand1 = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.HrsLabel = new DevExpress.XtraReports.UI.XRLabel();
			this.TotalLaborValue = new DevExpress.XtraReports.UI.XRLabel();
			this.TotalCostPriceValue = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
			this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GroupCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GroupData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailCaptionBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
			this.UseBlankReport = new DevExpress.XtraReports.UI.FormattingRule();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
			this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.printBlankReport = new DevExpress.XtraReports.Parameters.Parameter();
			this.quoteNo = new DevExpress.XtraReports.Parameters.Parameter();
			this.TotalCost = new DevExpress.XtraReports.UI.CalculatedField();
			this.TotalLaborTime = new DevExpress.XtraReports.UI.CalculatedField();
			((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
			this.Detail.HeightF = 18F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrTable3
			// 
			this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
			this.xrTable3.Name = "xrTable3";
			this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
			this.xrTable3.SizeF = new System.Drawing.SizeF(730.9901F, 18F);
			// 
			// xrTableRow3
			// 
			this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell3,
            this.xrTableCell46,
            this.xrTableCell4});
			this.xrTableRow3.Name = "xrTableRow3";
			this.xrTableRow3.Weight = 11.5D;
			// 
			// xrTableCell42
			// 
			this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Quantity", "{0:#,#}")});
			this.xrTableCell42.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell42.Name = "xrTableCell42";
			this.xrTableCell42.StyleName = "DetailData3";
			this.xrTableCell42.StylePriority.UseFont = false;
			this.xrTableCell42.StylePriority.UseTextAlignment = false;
			this.xrTableCell42.Text = "xrTableCell42";
			this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell42.Weight = 0.029463505348892083D;
			// 
			// xrTableCell43
			// 
			this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Model")});
			this.xrTableCell43.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell43.Name = "xrTableCell43";
			this.xrTableCell43.StyleName = "DetailData3";
			this.xrTableCell43.StylePriority.UseFont = false;
			this.xrTableCell43.StylePriority.UseTextAlignment = false;
			this.xrTableCell43.Text = "xrTableCell43";
			this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell43.Weight = 0.042432381884974474D;
			// 
			// xrTableCell44
			// 
			this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Description")});
			this.xrTableCell44.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell44.Name = "xrTableCell44";
			this.xrTableCell44.StyleName = "DetailData3";
			this.xrTableCell44.StylePriority.UseFont = false;
			this.xrTableCell44.StylePriority.UseTextAlignment = false;
			this.xrTableCell44.Text = "xrTableCell44";
			this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell44.Weight = 0.0856939567528815D;
			// 
			// xrTableCell3
			// 
			this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Cost", "{0:$#,##0.00}")});
			this.xrTableCell3.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell3.Name = "xrTableCell3";
			this.xrTableCell3.StylePriority.UseFont = false;
			this.xrTableCell3.StylePriority.UseTextAlignment = false;
			this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell3.Weight = 0.025697908032077665D;
			// 
			// xrTableCell46
			// 
			this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.ExtCost", "{0:$#,##0.00}")});
			this.xrTableCell46.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell46.Name = "xrTableCell46";
			this.xrTableCell46.StyleName = "DetailData3";
			this.xrTableCell46.StylePriority.UseFont = false;
			this.xrTableCell46.StylePriority.UseTextAlignment = false;
			this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell46.Weight = 0.034263875417371317D;
			// 
			// xrTableCell4
			// 
			this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Labor", "{0:#.00}")});
			this.xrTableCell4.Font = new System.Drawing.Font("Calibri", 9F);
			this.xrTableCell4.Name = "xrTableCell4";
			this.xrTableCell4.StylePriority.UseFont = false;
			this.xrTableCell4.StylePriority.UseTextAlignment = false;
			this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell4.Weight = 0.032913896639680508D;
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 25F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 25F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.StylePriority.UseTextAlignment = false;
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// dataSource
			// 
			this.dataSource.ConnectionName = "ConnectionString";
			this.dataSource.Name = "dataSource";
			storedProcQuery1.Name = "QuotationReport";
			queryParameter1.Name = "@quoteNo";
			queryParameter1.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.quoteNo]", typeof(decimal));
			queryParameter2.Name = "@printBlankReport";
			queryParameter2.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.printBlankReport]", typeof(bool));
			storedProcQuery1.Parameters.Add(queryParameter1);
			storedProcQuery1.Parameters.Add(queryParameter2);
			storedProcQuery1.StoredProcName = "QuotationReport";
			customSqlQuery1.Name = "CompanyInfo";
			customSqlQuery1.Sql = resources.GetString("customSqlQuery1.Sql");
			this.dataSource.Queries.AddRange(new DevExpress.DataAccess.Sql.SqlQuery[] {
            storedProcQuery1,
            customSqlQuery1});
			this.dataSource.ResultSchemaSerializable = resources.GetString("dataSource.ResultSchemaSerializable");
			// 
			// groupHeaderBand1
			// 
			this.groupHeaderBand1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("QuotNo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
			this.groupHeaderBand1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
			this.groupHeaderBand1.HeightF = 0F;
			this.groupHeaderBand1.Level = 1;
			this.groupHeaderBand1.Name = "groupHeaderBand1";
			// 
			// groupHeaderBand2
			// 
			this.groupHeaderBand2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
			this.groupHeaderBand2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
			this.groupHeaderBand2.HeightF = 28.4167F;
			this.groupHeaderBand2.Level = 2;
			this.groupHeaderBand2.Name = "groupHeaderBand2";
			// 
			// xrTable2
			// 
			this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999879F, 0F);
			this.xrTable2.Name = "xrTable2";
			this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
			this.xrTable2.SizeF = new System.Drawing.SizeF(731.0002F, 18F);
			// 
			// xrTableRow2
			// 
			this.xrTableRow2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Double;
			this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell1,
            this.xrTableCell22});
			this.xrTableRow2.Name = "xrTableRow2";
			this.xrTableRow2.StylePriority.UseBorderDashStyle = false;
			this.xrTableRow2.StylePriority.UseBorders = false;
			this.xrTableRow2.Weight = 1D;
			// 
			// xrTableCell18
			// 
			this.xrTableCell18.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell18.Name = "xrTableCell18";
			this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell18.StylePriority.UseFont = false;
			this.xrTableCell18.StylePriority.UsePadding = false;
			this.xrTableCell18.StylePriority.UseTextAlignment = false;
			this.xrTableCell18.Text = "QUANTITY";
			this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell18.Weight = 0.15246351100675712D;
			// 
			// xrTableCell19
			// 
			this.xrTableCell19.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell19.Name = "xrTableCell19";
			this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell19.StylePriority.UseFont = false;
			this.xrTableCell19.StylePriority.UsePadding = false;
			this.xrTableCell19.StylePriority.UseTextAlignment = false;
			this.xrTableCell19.Text = "MODEL";
			this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell19.Weight = 0.21956318681229695D;
			// 
			// xrTableCell20
			// 
			this.xrTableCell20.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell20.Name = "xrTableCell20";
			this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell20.StylePriority.UseFont = false;
			this.xrTableCell20.StylePriority.UsePadding = false;
			this.xrTableCell20.StylePriority.UseTextAlignment = false;
			this.xrTableCell20.Text = "DESCRIPTION";
			this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell20.Weight = 0.44341592141953023D;
			// 
			// xrTableCell21
			// 
			this.xrTableCell21.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell21.Name = "xrTableCell21";
			this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell21.StylePriority.UseFont = false;
			this.xrTableCell21.StylePriority.UsePadding = false;
			this.xrTableCell21.StylePriority.UseTextAlignment = false;
			this.xrTableCell21.Text = "COST";
			this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell21.Weight = 0.13297106309518214D;
			// 
			// xrTableCell1
			// 
			this.xrTableCell1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell1.Name = "xrTableCell1";
			this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell1.StylePriority.UseFont = false;
			this.xrTableCell1.StylePriority.UsePadding = false;
			this.xrTableCell1.StylePriority.UseTextAlignment = false;
			this.xrTableCell1.Text = "EXT. COST";
			this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell1.Weight = 0.17729474004004242D;
			// 
			// xrTableCell22
			// 
			this.xrTableCell22.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
			this.xrTableCell22.Name = "xrTableCell22";
			this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
			this.xrTableCell22.StylePriority.UseFont = false;
			this.xrTableCell22.StylePriority.UsePadding = false;
			this.xrTableCell22.StylePriority.UseTextAlignment = false;
			this.xrTableCell22.Text = "LABOR TIME";
			this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell22.Weight = 0.1703165776477894D;
			// 
			// groupFooterBand1
			// 
			this.groupFooterBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.HrsLabel,
            this.TotalLaborValue,
            this.TotalCostPriceValue,
            this.xrLabel42,
            this.xrLabel32});
			this.groupFooterBand1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
			this.groupFooterBand1.HeightF = 54.45503F;
			this.groupFooterBand1.Name = "groupFooterBand1";
			this.groupFooterBand1.PrintAtBottom = true;
			// 
			// HrsLabel
			// 
			this.HrsLabel.LocationFloat = new DevExpress.Utils.PointFloat(711.9401F, 0F);
			this.HrsLabel.Name = "HrsLabel";
			this.HrsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.HrsLabel.SizeF = new System.Drawing.SizeF(29.06F, 18.00001F);
			this.HrsLabel.StylePriority.UseTextAlignment = false;
			this.HrsLabel.Text = "hrs.";
			this.HrsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// TotalLaborValue
			// 
			this.TotalLaborValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.TotalLaborTime", "{0:#.00}")});
			this.TotalLaborValue.LocationFloat = new DevExpress.Utils.PointFloat(644.936F, 0F);
			this.TotalLaborValue.Name = "TotalLaborValue";
			this.TotalLaborValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.TotalLaborValue.SizeF = new System.Drawing.SizeF(67F, 18F);
			this.TotalLaborValue.StylePriority.UseTextAlignment = false;
			xrSummary1.FormatString = "{0:#.00}";
			xrSummary1.IgnoreNullValues = true;
			this.TotalLaborValue.Summary = xrSummary1;
			this.TotalLaborValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// TotalCostPriceValue
			// 
			this.TotalCostPriceValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.TotalCost", "{0:$#,##0.00}")});
			this.TotalCostPriceValue.LocationFloat = new DevExpress.Utils.PointFloat(544.93F, 0F);
			this.TotalCostPriceValue.Name = "TotalCostPriceValue";
			this.TotalCostPriceValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.TotalCostPriceValue.SizeF = new System.Drawing.SizeF(100F, 18F);
			this.TotalCostPriceValue.StylePriority.UseTextAlignment = false;
			xrSummary2.FormatString = "{0:$#,##0.00}";
			xrSummary2.IgnoreNullValues = true;
			this.TotalCostPriceValue.Summary = xrSummary2;
			this.TotalCostPriceValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel42
			// 
			this.xrLabel42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.QuoteTotal", "{0:$#,##0.00}")});
			this.xrLabel42.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(614.9584F, 36.45503F);
			this.xrLabel42.Name = "xrLabel42";
			this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel42.SizeF = new System.Drawing.SizeF(126.0416F, 18F);
			this.xrLabel42.StylePriority.UseFont = false;
			this.xrLabel42.StylePriority.UseTextAlignment = false;
			this.xrLabel42.Text = "xrLabel42";
			this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel32
			// 
			this.xrLabel32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 36.45503F);
			this.xrLabel32.Name = "xrLabel32";
			this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel32.SizeF = new System.Drawing.SizeF(468.45F, 18F);
			this.xrLabel32.StylePriority.UseFont = false;
			this.xrLabel32.StylePriority.UseTextAlignment = false;
			this.xrLabel32.Text = "TOTAL PRICE FOR SYSTEM INSTALLED ************************************";
			this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// Title
			// 
			this.Title.BackColor = System.Drawing.Color.Transparent;
			this.Title.BorderColor = System.Drawing.Color.Black;
			this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.Title.BorderWidth = 1F;
			this.Title.Font = new System.Drawing.Font("Tahoma", 14F);
			this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.Title.Name = "Title";
			// 
			// GroupCaption3
			// 
			this.GroupCaption3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
			this.GroupCaption3.BorderColor = System.Drawing.Color.White;
			this.GroupCaption3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.GroupCaption3.BorderWidth = 2F;
			this.GroupCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GroupCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
			this.GroupCaption3.Name = "GroupCaption3";
			this.GroupCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.GroupCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// GroupData3
			// 
			this.GroupData3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
			this.GroupData3.BorderColor = System.Drawing.Color.White;
			this.GroupData3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.GroupData3.BorderWidth = 2F;
			this.GroupData3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GroupData3.ForeColor = System.Drawing.Color.White;
			this.GroupData3.Name = "GroupData3";
			this.GroupData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.GroupData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailCaption3
			// 
			this.DetailCaption3.BackColor = System.Drawing.Color.Transparent;
			this.DetailCaption3.BorderColor = System.Drawing.Color.Transparent;
			this.DetailCaption3.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.DetailCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.DetailCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.DetailCaption3.Name = "DetailCaption3";
			this.DetailCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailData3
			// 
			this.DetailData3.Font = new System.Drawing.Font("Tahoma", 8F);
			this.DetailData3.ForeColor = System.Drawing.Color.Black;
			this.DetailData3.Name = "DetailData3";
			this.DetailData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailData3_Odd
			// 
			this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
			this.DetailData3_Odd.BorderColor = System.Drawing.Color.Transparent;
			this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.DetailData3_Odd.BorderWidth = 1F;
			this.DetailData3_Odd.Font = new System.Drawing.Font("Tahoma", 8F);
			this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
			this.DetailData3_Odd.Name = "DetailData3_Odd";
			this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailCaptionBackground3
			// 
			this.DetailCaptionBackground3.BackColor = System.Drawing.Color.Transparent;
			this.DetailCaptionBackground3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(206)))), ((int)(((byte)(206)))));
			this.DetailCaptionBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
			this.DetailCaptionBackground3.BorderWidth = 2F;
			this.DetailCaptionBackground3.Name = "DetailCaptionBackground3";
			// 
			// PageInfo
			// 
			this.PageInfo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.PageInfo.Name = "PageInfo";
			this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel43,
            this.xrPageInfo3,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel5,
            this.xrPictureBox1,
            this.xrLabel6,
            this.xrLabel14,
            this.xrLabel20});
			this.PageHeader.HeightF = 381.4583F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrLabel43
			// 
			this.xrLabel43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.CustFax1")});
			this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 296.2083F);
			this.xrLabel43.Name = "xrLabel43";
			this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel43.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel43.Text = "xrLabel43";
			// 
			// xrPageInfo3
			// 
			this.xrPageInfo3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo3.ForeColor = System.Drawing.Color.Black;
			this.xrPageInfo3.Format = "PAGE: {0}";
			this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(428F, 363.4583F);
			this.xrPageInfo3.Name = "xrPageInfo3";
			this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
			this.xrPageInfo3.SizeF = new System.Drawing.SizeF(312.9999F, 18F);
			this.xrPageInfo3.StyleName = "PageInfo";
			this.xrPageInfo3.StylePriority.UseFont = false;
			this.xrPageInfo3.StylePriority.UseForeColor = false;
			this.xrPageInfo3.StylePriority.UsePadding = false;
			this.xrPageInfo3.StylePriority.UseTextAlignment = false;
			this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel31
			// 
			this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.System")});
			this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(76.25004F, 345.4583F);
			this.xrLabel31.Name = "xrLabel31";
			this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel31.SizeF = new System.Drawing.SizeF(401.0417F, 18F);
			this.xrLabel31.Text = "xrLabel31";
			// 
			// xrLabel30
			// 
			this.xrLabel30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(66.25004F, 345.4583F);
			this.xrLabel30.Name = "xrLabel30";
			this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel30.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel30.StylePriority.UseFont = false;
			this.xrLabel30.Text = ":";
			// 
			// xrLabel29
			// 
			this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(12.08337F, 345.4583F);
			this.xrLabel29.Name = "xrLabel29";
			this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel29.SizeF = new System.Drawing.SizeF(54.16664F, 18F);
			this.xrLabel29.Text = "SYSTEM";
			// 
			// xrLabel28
			// 
			this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.ProjName")});
			this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(76.25001F, 327.4584F);
			this.xrLabel28.Name = "xrLabel28";
			this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel28.SizeF = new System.Drawing.SizeF(401.0417F, 18F);
			this.xrLabel28.Text = "xrLabel28";
			// 
			// xrLabel27
			// 
			this.xrLabel27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(66.25001F, 327.4583F);
			this.xrLabel27.Name = "xrLabel27";
			this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel27.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel27.StylePriority.UseFont = false;
			this.xrLabel27.Text = ":";
			// 
			// xrLabel26
			// 
			this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(12.08334F, 327.4584F);
			this.xrLabel26.Name = "xrLabel26";
			this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel26.SizeF = new System.Drawing.SizeF(54.16667F, 18F);
			this.xrLabel26.Text = "PROJECT";
			// 
			// xrLabel25
			// 
			this.xrLabel25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(588.0237F, 296.2083F);
			this.xrLabel25.Name = "xrLabel25";
			this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel25.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel25.StylePriority.UseFont = false;
			this.xrLabel25.Text = ":";
			// 
			// xrLabel24
			// 
			this.xrLabel24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(588.0236F, 278.2084F);
			this.xrLabel24.Name = "xrLabel24";
			this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel24.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel24.StylePriority.UseFont = false;
			this.xrLabel24.Text = ":";
			// 
			// xrLabel23
			// 
			this.xrLabel23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(588.0236F, 260.2084F);
			this.xrLabel23.Name = "xrLabel23";
			this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel23.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel23.StylePriority.UseFont = false;
			this.xrLabel23.Text = ":";
			// 
			// xrLabel22
			// 
			this.xrLabel22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(588.0235F, 242.2084F);
			this.xrLabel22.Name = "xrLabel22";
			this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel22.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel22.StylePriority.UseFont = false;
			this.xrLabel22.Text = ":";
			// 
			// xrLabel21
			// 
			this.xrLabel21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(588.0236F, 224.2084F);
			this.xrLabel21.Name = "xrLabel21";
			this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel21.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel21.StylePriority.UseFont = false;
			this.xrLabel21.Text = ":";
			// 
			// xrLabel19
			// 
			this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(499.4819F, 296.2083F);
			this.xrLabel19.Name = "xrLabel19";
			this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel19.SizeF = new System.Drawing.SizeF(88.54163F, 18F);
			this.xrLabel19.Text = "F.O.B.";
			// 
			// xrLabel18
			// 
			this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(499.4819F, 278.2084F);
			this.xrLabel18.Name = "xrLabel18";
			this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel18.SizeF = new System.Drawing.SizeF(88.54163F, 18F);
			this.xrLabel18.Text = "SHIPPED VIA";
			// 
			// xrLabel17
			// 
			this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(499.4819F, 260.2084F);
			this.xrLabel17.Name = "xrLabel17";
			this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel17.SizeF = new System.Drawing.SizeF(88.54163F, 18F);
			this.xrLabel17.Text = "TERMS";
			// 
			// xrLabel16
			// 
			this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(499.482F, 242.2084F);
			this.xrLabel16.Name = "xrLabel16";
			this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel16.SizeF = new System.Drawing.SizeF(88.54156F, 18F);
			this.xrLabel16.Text = "DELIVERY DATE";
			// 
			// xrLabel15
			// 
			this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(499.4819F, 224.2084F);
			this.xrLabel15.Name = "xrLabel15";
			this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel15.SizeF = new System.Drawing.SizeF(88.54163F, 18F);
			this.xrLabel15.Text = "SALESPERSON";
			// 
			// xrLabel13
			// 
			this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.FOB")});
			this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(598.0237F, 296.2084F);
			this.xrLabel13.Name = "xrLabel13";
			this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel13.SizeF = new System.Drawing.SizeF(142.9762F, 18F);
			this.xrLabel13.Text = "xrLabel13";
			// 
			// xrLabel12
			// 
			this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Shipped")});
			this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(598.0237F, 278.2084F);
			this.xrLabel12.Name = "xrLabel12";
			this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel12.SizeF = new System.Drawing.SizeF(142.9762F, 18F);
			this.xrLabel12.Text = "xrLabel12";
			// 
			// xrLabel11
			// 
			this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Term")});
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(598.0239F, 260.2084F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(142.9762F, 18F);
			this.xrLabel11.Text = "xrLabel11";
			// 
			// xrLabel10
			// 
			this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.DelvDate")});
			this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(598.0236F, 242.2084F);
			this.xrLabel10.Name = "xrLabel10";
			this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel10.SizeF = new System.Drawing.SizeF(142.9763F, 18F);
			this.xrLabel10.Text = "xrLabel10";
			// 
			// xrLabel9
			// 
			this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.SalePerson")});
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(598.0236F, 224.2084F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(142.9763F, 18F);
			this.xrLabel9.Text = "xrLabel9";
			// 
			// xrLabel8
			// 
			this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Date")});
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(598.0236F, 206.2084F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(142.9763F, 18F);
			this.xrLabel8.Text = "xrLabel8";
			// 
			// xrLabel7
			// 
			this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.CustPhone1")});
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 278.2084F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel7.Text = "xrLabel7";
			// 
			// xrLabel4
			// 
			this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.CustAdd2")});
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 242.2084F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel4.Text = "xrLabel4";
			// 
			// xrLabel3
			// 
			this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.CustAdd1")});
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 224.2084F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel3.Text = "xrLabel3";
			// 
			// xrLabel1
			// 
			this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.CustName")});
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 206.2084F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel1.Text = "xrLabel1";
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 149.13F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(731F, 30.00002F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "QUOTATION :   [QuotNo]";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrPictureBox1
			// 
			this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.xrPictureBox1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Image", null, "CompanyInfo.ReportLogo")});
			this.xrPictureBox1.FormattingRules.Add(this.UseBlankReport);
			this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
			this.xrPictureBox1.Name = "xrPictureBox1";
			this.xrPictureBox1.SizeF = new System.Drawing.SizeF(290.4165F, 89.66666F);
			this.xrPictureBox1.StylePriority.UseBorders = false;
			// 
			// UseBlankReport
			// 
			this.UseBlankReport.Condition = "[Parameters.printBlankReport]==True";
			this.UseBlankReport.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
			this.UseBlankReport.Name = "UseBlankReport";
			// 
			// xrLabel6
			// 
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 260.2084F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(290.4165F, 18F);
			this.xrLabel6.Text = "[CustCity], [CustState] [CustZipcode]";
			// 
			// xrLabel14
			// 
			this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(499.4819F, 206.2084F);
			this.xrLabel14.Name = "xrLabel14";
			this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel14.SizeF = new System.Drawing.SizeF(88.54163F, 18F);
			this.xrLabel14.Text = "DATE";
			// 
			// xrLabel20
			// 
			this.xrLabel20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
			this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(588.0236F, 206.2084F);
			this.xrLabel20.Name = "xrLabel20";
			this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel20.SizeF = new System.Drawing.SizeF(10F, 18F);
			this.xrLabel20.StylePriority.UseFont = false;
			this.xrLabel20.Text = ":";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel39,
            this.xrLabel40,
            this.xrLabel38,
            this.xrLabel35,
            this.xrLabel36,
            this.xrLabel37,
            this.xrPanel3});
			this.PageFooter.HeightF = 187.5416F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrLabel39
			// 
			this.xrLabel39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CompanyInfo.Name")});
			this.xrLabel39.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel39.ForeColor = System.Drawing.Color.Red;
			this.xrLabel39.FormattingRules.Add(this.UseBlankReport);
			this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(0.9999593F, 169.5416F);
			this.xrLabel39.Name = "xrLabel39";
			this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel39.SizeF = new System.Drawing.SizeF(206.154F, 18F);
			this.xrLabel39.StylePriority.UseFont = false;
			this.xrLabel39.StylePriority.UseForeColor = false;
			this.xrLabel39.StylePriority.UseTextAlignment = false;
			this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel40
			// 
			this.xrLabel40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CompanyInfo.Info")});
			this.xrLabel40.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel40.ForeColor = System.Drawing.Color.Gray;
			this.xrLabel40.FormattingRules.Add(this.UseBlankReport);
			this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(207.154F, 169.5416F);
			this.xrLabel40.Name = "xrLabel40";
			this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel40.SizeF = new System.Drawing.SizeF(543.846F, 18F);
			this.xrLabel40.StylePriority.UseFont = false;
			this.xrLabel40.StylePriority.UseForeColor = false;
			this.xrLabel40.StylePriority.UseTextAlignment = false;
			this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			this.xrLabel40.WordWrap = false;
			// 
			// xrLabel38
			// 
			this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(478.5002F, 115.625F);
			this.xrLabel38.Name = "xrLabel38";
			this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel38.SizeF = new System.Drawing.SizeF(262.5F, 23F);
			this.xrLabel38.Text = "BY: [By]";
			// 
			// xrLabel35
			// 
			this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.xrLabel35.BorderWidth = 3F;
			this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(9.999879F, 87.62499F);
			this.xrLabel35.Name = "xrLabel35";
			this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel35.SizeF = new System.Drawing.SizeF(731F, 18F);
			this.xrLabel35.StylePriority.UseBorders = false;
			this.xrLabel35.StylePriority.UseBorderWidth = false;
			this.xrLabel35.Text = "TO CONFIRM ORDER, PLEASE SIGN AND RETURN WHITE ACCEPTANCE COPY.";
			// 
			// xrLabel36
			// 
			this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 115.625F);
			this.xrLabel36.Name = "xrLabel36";
			this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel36.SizeF = new System.Drawing.SizeF(262.5F, 23F);
			this.xrLabel36.Text = "QUOTE VALID FOR 30 DAYS";
			// 
			// xrLabel37
			// 
			this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 138.625F);
			this.xrLabel37.Name = "xrLabel37";
			this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel37.SizeF = new System.Drawing.SizeF(262.5F, 23F);
			this.xrLabel37.Text = "ACCEPTED BY: [AcceptedBy]";
			// 
			// xrPanel3
			// 
			this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel48,
            this.xrLabel47,
            this.xrLabel46,
            this.xrLabel2});
			this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(49.95832F, 0F);
			this.xrPanel3.Name = "xrPanel3";
			this.xrPanel3.SizeF = new System.Drawing.SizeF(651.0416F, 72F);
			this.xrPanel3.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			// 
			// xrLabel48
			// 
			this.xrLabel48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Notes4")});
			this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 53.99997F);
			this.xrLabel48.Name = "xrLabel48";
			this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel48.SizeF = new System.Drawing.SizeF(651.0416F, 18F);
			// 
			// xrLabel47
			// 
			this.xrLabel47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Notes3")});
			this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 36.00006F);
			this.xrLabel47.Name = "xrLabel47";
			this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel47.SizeF = new System.Drawing.SizeF(651.0416F, 18F);
			// 
			// xrLabel46
			// 
			this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Notes2")});
			this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 18.00003F);
			this.xrLabel46.Name = "xrLabel46";
			this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel46.SizeF = new System.Drawing.SizeF(651.0416F, 18F);
			// 
			// xrLabel2
			// 
			this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuotationReport.Notes1")});
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(651.0416F, 18F);
			// 
			// printBlankReport
			// 
			this.printBlankReport.Name = "printBlankReport";
			this.printBlankReport.Type = typeof(bool);
			this.printBlankReport.ValueInfo = "False";
			// 
			// quoteNo
			// 
			this.quoteNo.Name = "quoteNo";
			this.quoteNo.Type = typeof(decimal);
			this.quoteNo.ValueInfo = "29930";
			// 
			// TotalCost
			// 
			this.TotalCost.DataMember = "QuotationReport";
			this.TotalCost.Expression = "Sum([ExtCost])";
			this.TotalCost.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
			this.TotalCost.Name = "TotalCost";
			// 
			// TotalLaborTime
			// 
			this.TotalLaborTime.DataMember = "QuotationReport";
			this.TotalLaborTime.Expression = "Sum([Labor])";
			this.TotalLaborTime.FieldType = DevExpress.XtraReports.UI.FieldType.Decimal;
			this.TotalLaborTime.Name = "TotalLaborTime";
			// 
			// QuotationCostLaborReport
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.groupHeaderBand1,
            this.groupHeaderBand2,
            this.groupFooterBand1,
            this.PageHeader,
            this.PageFooter});
			this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.TotalCost,
            this.TotalLaborTime});
			this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.dataSource});
			this.DataMember = "QuotationReport";
			this.DataSource = this.dataSource;
			this.Font = new System.Drawing.Font("Calibri", 9.75F);
			this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.UseBlankReport});
			this.Margins = new System.Drawing.Printing.Margins(49, 50, 25, 25);
			this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.quoteNo,
            this.printBlankReport});
			this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.GroupCaption3,
            this.GroupData3,
            this.DetailCaption3,
            this.DetailData3,
            this.DetailData3_Odd,
            this.DetailCaptionBackground3,
            this.PageInfo});
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRTable xrTable3;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
		private DevExpress.DataAccess.Sql.SqlDataSource dataSource;
		private DevExpress.XtraReports.UI.GroupHeaderBand groupHeaderBand1;
		private DevExpress.XtraReports.UI.GroupHeaderBand groupHeaderBand2;
		private DevExpress.XtraReports.UI.GroupFooterBand groupFooterBand1;
		private DevExpress.XtraReports.UI.XRControlStyle Title;
		private DevExpress.XtraReports.UI.XRControlStyle GroupCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle GroupData3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailData3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
		private DevExpress.XtraReports.UI.XRControlStyle DetailCaptionBackground3;
		private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel xrLabel31;
		private DevExpress.XtraReports.UI.XRLabel xrLabel30;
		private DevExpress.XtraReports.UI.XRLabel xrLabel29;
		private DevExpress.XtraReports.UI.XRLabel xrLabel28;
		private DevExpress.XtraReports.UI.XRLabel xrLabel27;
		private DevExpress.XtraReports.UI.XRLabel xrLabel26;
		private DevExpress.XtraReports.UI.XRLabel xrLabel25;
		private DevExpress.XtraReports.UI.XRLabel xrLabel24;
		private DevExpress.XtraReports.UI.XRLabel xrLabel23;
		private DevExpress.XtraReports.UI.XRLabel xrLabel22;
		private DevExpress.XtraReports.UI.XRLabel xrLabel21;
		private DevExpress.XtraReports.UI.XRLabel xrLabel19;
		private DevExpress.XtraReports.UI.XRLabel xrLabel18;
		private DevExpress.XtraReports.UI.XRLabel xrLabel17;
		private DevExpress.XtraReports.UI.XRLabel xrLabel16;
		private DevExpress.XtraReports.UI.XRLabel xrLabel15;
		private DevExpress.XtraReports.UI.XRLabel xrLabel13;
		private DevExpress.XtraReports.UI.XRLabel xrLabel12;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.XRLabel xrLabel10;
		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel14;
		private DevExpress.XtraReports.UI.XRLabel xrLabel20;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
		private DevExpress.XtraReports.UI.XRTable xrTable2;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
		private DevExpress.XtraReports.UI.XRLabel TotalLaborValue;
		private DevExpress.XtraReports.UI.XRLabel TotalCostPriceValue;
		private DevExpress.XtraReports.UI.XRLabel xrLabel32;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRLabel xrLabel35;
		private DevExpress.XtraReports.UI.XRLabel xrLabel38;
		private DevExpress.XtraReports.UI.XRLabel xrLabel36;
		private DevExpress.XtraReports.UI.XRLabel xrLabel37;
		private DevExpress.XtraReports.Parameters.Parameter printBlankReport;
		private DevExpress.XtraReports.Parameters.Parameter quoteNo;
		private DevExpress.XtraReports.UI.XRLabel xrLabel39;
		private DevExpress.XtraReports.UI.XRLabel xrLabel40;
		private DevExpress.XtraReports.UI.XRLabel HrsLabel;
		private DevExpress.XtraReports.UI.FormattingRule UseBlankReport;
		private DevExpress.XtraReports.UI.XRLabel xrLabel42;
		private DevExpress.XtraReports.UI.XRLabel xrLabel43;
		private DevExpress.XtraReports.UI.XRPanel xrPanel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel48;
		private DevExpress.XtraReports.UI.XRLabel xrLabel47;
		private DevExpress.XtraReports.UI.XRLabel xrLabel46;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
		private DevExpress.XtraReports.UI.CalculatedField TotalCost;
		private DevExpress.XtraReports.UI.CalculatedField TotalLaborTime;
	}
}
