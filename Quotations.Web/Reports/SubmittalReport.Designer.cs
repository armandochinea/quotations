﻿namespace Quotations.Web.Reports
{
	partial class SubmittalReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.DataAccess.Sql.StoredProcQuery storedProcQuery1 = new DevExpress.DataAccess.Sql.StoredProcQuery();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter1 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter2 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter3 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.QueryParameter queryParameter4 = new DevExpress.DataAccess.Sql.QueryParameter();
			DevExpress.DataAccess.Sql.CustomSqlQuery customSqlQuery1 = new DevExpress.DataAccess.Sql.CustomSqlQuery();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubmittalReport));
			DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
			this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
			this.HideQty = new DevExpress.XtraReports.UI.FormattingRule();
			this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
			this.HideLaborTime = new DevExpress.XtraReports.UI.FormattingRule();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.dataSource = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.groupHeaderBand1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.groupHeaderBand2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
			this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
			this.groupFooterBand1 = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.groupFooterBand2 = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.reportFooterBand1 = new DevExpress.XtraReports.UI.ReportFooterBand();
			this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GroupCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GroupData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
			this.DetailCaptionBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.TotalCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.TotalData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.TotalBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GrandTotalCaption3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GrandTotalData3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.GrandTotalBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
			this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
			this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
			this.UseBlankReport = new DevExpress.XtraReports.UI.FormattingRule();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.quoteNo = new DevExpress.XtraReports.Parameters.Parameter();
			this.printBlankReport = new DevExpress.XtraReports.Parameters.Parameter();
			this.printQtyField = new DevExpress.XtraReports.Parameters.Parameter();
			this.printLaborTimeField = new DevExpress.XtraReports.Parameters.Parameter();
			((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
			this.Detail.HeightF = 27.99997F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrTable3
			// 
			this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(10.00039F, 9.999974F);
			this.xrTable3.Name = "xrTable3";
			this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
			this.xrTable3.SizeF = new System.Drawing.SizeF(729.9998F, 18F);
			// 
			// xrTableRow3
			// 
			this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18});
			this.xrTableRow3.Name = "xrTableRow3";
			this.xrTableRow3.Weight = 11.5D;
			// 
			// xrTableCell15
			// 
			this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Quantity", "{0:#,#}")});
			this.xrTableCell15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell15.FormattingRules.Add(this.HideQty);
			this.xrTableCell15.Name = "xrTableCell15";
			this.xrTableCell15.StyleName = "DetailData3";
			this.xrTableCell15.StylePriority.UseFont = false;
			this.xrTableCell15.StylePriority.UseTextAlignment = false;
			this.xrTableCell15.Text = "xrTableCell15";
			this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			this.xrTableCell15.Weight = 0.05679615751697617D;
			// 
			// HideQty
			// 
			this.HideQty.Condition = "[Parameters.printQtyField]==False";
			this.HideQty.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
			this.HideQty.Name = "HideQty";
			// 
			// xrTableCell16
			// 
			this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Model")});
			this.xrTableCell16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell16.Name = "xrTableCell16";
			this.xrTableCell16.StyleName = "DetailData3";
			this.xrTableCell16.StylePriority.UseFont = false;
			this.xrTableCell16.Text = "xrTableCell16";
			this.xrTableCell16.Weight = 0.14199037948996773D;
			// 
			// xrTableCell17
			// 
			this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Description")});
			this.xrTableCell17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell17.Name = "xrTableCell17";
			this.xrTableCell17.StyleName = "DetailData3";
			this.xrTableCell17.StylePriority.UseFont = false;
			this.xrTableCell17.Text = "xrTableCell17";
			this.xrTableCell17.Weight = 0.24848316926763872D;
			// 
			// xrTableCell18
			// 
			this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Labor", "{0:#.00}")});
			this.xrTableCell18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell18.FormattingRules.Add(this.HideLaborTime);
			this.xrTableCell18.Name = "xrTableCell18";
			this.xrTableCell18.StyleName = "DetailData3";
			this.xrTableCell18.StylePriority.UseFont = false;
			this.xrTableCell18.StylePriority.UseTextAlignment = false;
			this.xrTableCell18.Text = "xrTableCell18";
			this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell18.Weight = 0.070995075970759935D;
			// 
			// HideLaborTime
			// 
			this.HideLaborTime.Condition = "[Parameters.printLaborTimeField]==False";
			this.HideLaborTime.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
			this.HideLaborTime.Name = "HideLaborTime";
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 25F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 25F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// dataSource
			// 
			this.dataSource.ConnectionName = "ConnectionString";
			this.dataSource.Name = "dataSource";
			storedProcQuery1.Name = "SubmittalReport";
			queryParameter1.Name = "@quoteNo";
			queryParameter1.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.quoteNo]", typeof(int));
			queryParameter2.Name = "@printBlankReport";
			queryParameter2.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.printBlankReport]", typeof(bool));
			queryParameter3.Name = "@printQtyField";
			queryParameter3.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.printQtyField]", typeof(bool));
			queryParameter4.Name = "@printLaborTimeField";
			queryParameter4.Type = typeof(DevExpress.DataAccess.Expression);
			queryParameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.printLaborTimeField]", typeof(bool));
			storedProcQuery1.Parameters.Add(queryParameter1);
			storedProcQuery1.Parameters.Add(queryParameter2);
			storedProcQuery1.Parameters.Add(queryParameter3);
			storedProcQuery1.Parameters.Add(queryParameter4);
			storedProcQuery1.StoredProcName = "SubmittalReport";
			customSqlQuery1.Name = "CompanyInfo";
			customSqlQuery1.Sql = resources.GetString("customSqlQuery1.Sql");
			this.dataSource.Queries.AddRange(new DevExpress.DataAccess.Sql.SqlQuery[] {
            storedProcQuery1,
            customSqlQuery1});
			this.dataSource.ResultSchemaSerializable = resources.GetString("dataSource.ResultSchemaSerializable");
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
			this.xrPageInfo2.Format = "PAGE: {0}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(426.9997F, 287.8911F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 6, 0, 0, 100F);
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(313F, 23F);
			this.xrPageInfo2.StyleName = "PageInfo";
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseForeColor = false;
			this.xrPageInfo2.StylePriority.UsePadding = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// groupHeaderBand1
			// 
			this.groupHeaderBand1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("SubmittalNumber", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
			this.groupHeaderBand1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
			this.groupHeaderBand1.HeightF = 0F;
			this.groupHeaderBand1.Level = 1;
			this.groupHeaderBand1.Name = "groupHeaderBand1";
			// 
			// groupHeaderBand2
			// 
			this.groupHeaderBand2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
			this.groupHeaderBand2.HeightF = 0F;
			this.groupHeaderBand2.Level = 2;
			this.groupHeaderBand2.Name = "groupHeaderBand2";
			// 
			// xrPanel1
			// 
			this.xrPanel1.BorderColor = System.Drawing.Color.Black;
			this.xrPanel1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Double;
			this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(9.999813F, 323.3942F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.xrPanel1.SizeF = new System.Drawing.SizeF(730F, 29.80765F);
			this.xrPanel1.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.xrPanel1.StyleName = "DetailCaptionBackground3";
			this.xrPanel1.StylePriority.UseBorderColor = false;
			this.xrPanel1.StylePriority.UseBorderDashStyle = false;
			this.xrPanel1.StylePriority.UseBorders = false;
			this.xrPanel1.StylePriority.UsePadding = false;
			// 
			// xrTable2
			// 
			this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999992F);
			this.xrTable2.Name = "xrTable2";
			this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 1, 1, 100F);
			this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
			this.xrTable2.SizeF = new System.Drawing.SizeF(729.9999F, 20F);
			this.xrTable2.StylePriority.UsePadding = false;
			// 
			// xrTableRow2
			// 
			this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
			this.xrTableRow2.Name = "xrTableRow2";
			this.xrTableRow2.Weight = 1D;
			// 
			// xrTableCell7
			// 
			this.xrTableCell7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell7.ForeColor = System.Drawing.Color.Black;
			this.xrTableCell7.FormattingRules.Add(this.HideQty);
			this.xrTableCell7.Name = "xrTableCell7";
			this.xrTableCell7.StyleName = "DetailCaption3";
			this.xrTableCell7.StylePriority.UseFont = false;
			this.xrTableCell7.StylePriority.UseForeColor = false;
			this.xrTableCell7.StylePriority.UseTextAlignment = false;
			this.xrTableCell7.Text = "QUANTITY";
			this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			this.xrTableCell7.Weight = 0.12307689155836329D;
			// 
			// xrTableCell8
			// 
			this.xrTableCell8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell8.ForeColor = System.Drawing.Color.Black;
			this.xrTableCell8.Name = "xrTableCell8";
			this.xrTableCell8.StyleName = "DetailCaption3";
			this.xrTableCell8.StylePriority.UseFont = false;
			this.xrTableCell8.StylePriority.UseForeColor = false;
			this.xrTableCell8.Text = "MODEL";
			this.xrTableCell8.Weight = 0.30769223798184969D;
			// 
			// xrTableCell9
			// 
			this.xrTableCell9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell9.ForeColor = System.Drawing.Color.Black;
			this.xrTableCell9.Name = "xrTableCell9";
			this.xrTableCell9.StyleName = "DetailCaption3";
			this.xrTableCell9.StylePriority.UseFont = false;
			this.xrTableCell9.StylePriority.UseForeColor = false;
			this.xrTableCell9.Text = "DESCRIPTION";
			this.xrTableCell9.Weight = 0.53846141999317776D;
			// 
			// xrTableCell10
			// 
			this.xrTableCell10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell10.ForeColor = System.Drawing.Color.Black;
			this.xrTableCell10.FormattingRules.Add(this.HideLaborTime);
			this.xrTableCell10.Name = "xrTableCell10";
			this.xrTableCell10.StyleName = "DetailCaption3";
			this.xrTableCell10.StylePriority.UseFont = false;
			this.xrTableCell10.StylePriority.UseForeColor = false;
			this.xrTableCell10.StylePriority.UseTextAlignment = false;
			this.xrTableCell10.Text = "LABOR";
			this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrTableCell10.Weight = 0.15384592953325521D;
			// 
			// groupFooterBand1
			// 
			this.groupFooterBand1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
			this.groupFooterBand1.HeightF = 0F;
			this.groupFooterBand1.Name = "groupFooterBand1";
			// 
			// groupFooterBand2
			// 
			this.groupFooterBand2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel3});
			this.groupFooterBand2.HeightF = 36F;
			this.groupFooterBand2.KeepTogether = true;
			this.groupFooterBand2.Level = 1;
			this.groupFooterBand2.Name = "groupFooterBand2";
			this.groupFooterBand2.PrintAtBottom = true;
			// 
			// xrLabel4
			// 
			this.xrLabel4.CanGrow = false;
			this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Labor")});
			this.xrLabel4.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.ForeColor = System.Drawing.Color.Black;
			this.xrLabel4.FormattingRules.Add(this.HideLaborTime);
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(640.0002F, 0F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(99.99988F, 18F);
			this.xrLabel4.StyleName = "TotalData3";
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.StylePriority.UseForeColor = false;
			this.xrLabel4.StylePriority.UseTextAlignment = false;
			xrSummary1.FormatString = "{0:n}";
			xrSummary1.IgnoreNullValues = true;
			xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
			this.xrLabel4.Summary = xrSummary1;
			this.xrLabel4.Text = "xrLabel4";
			this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			this.xrLabel4.WordWrap = false;
			// 
			// xrLabel3
			// 
			this.xrLabel3.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.Black;
			this.xrLabel3.FormattingRules.Add(this.HideLaborTime);
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(502.7304F, 0F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(137.2696F, 18F);
			this.xrLabel3.StyleName = "TotalCaption3";
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.StylePriority.UseTextAlignment = false;
			this.xrLabel3.Text = "TOTAL LABOR TIME:";
			this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// reportFooterBand1
			// 
			this.reportFooterBand1.HeightF = 0F;
			this.reportFooterBand1.Name = "reportFooterBand1";
			// 
			// Title
			// 
			this.Title.BackColor = System.Drawing.Color.Transparent;
			this.Title.BorderColor = System.Drawing.Color.Black;
			this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.Title.BorderWidth = 1F;
			this.Title.Font = new System.Drawing.Font("Tahoma", 14F);
			this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.Title.Name = "Title";
			// 
			// GroupCaption3
			// 
			this.GroupCaption3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
			this.GroupCaption3.BorderColor = System.Drawing.Color.White;
			this.GroupCaption3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.GroupCaption3.BorderWidth = 2F;
			this.GroupCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GroupCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
			this.GroupCaption3.Name = "GroupCaption3";
			this.GroupCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.GroupCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// GroupData3
			// 
			this.GroupData3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
			this.GroupData3.BorderColor = System.Drawing.Color.White;
			this.GroupData3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.GroupData3.BorderWidth = 2F;
			this.GroupData3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GroupData3.ForeColor = System.Drawing.Color.White;
			this.GroupData3.Name = "GroupData3";
			this.GroupData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.GroupData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailCaption3
			// 
			this.DetailCaption3.BackColor = System.Drawing.Color.Transparent;
			this.DetailCaption3.BorderColor = System.Drawing.Color.Transparent;
			this.DetailCaption3.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.DetailCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.DetailCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.DetailCaption3.Name = "DetailCaption3";
			this.DetailCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailData3
			// 
			this.DetailData3.Font = new System.Drawing.Font("Tahoma", 8F);
			this.DetailData3.ForeColor = System.Drawing.Color.Black;
			this.DetailData3.Name = "DetailData3";
			this.DetailData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailData3_Odd
			// 
			this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
			this.DetailData3_Odd.BorderColor = System.Drawing.Color.Transparent;
			this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.DetailData3_Odd.BorderWidth = 1F;
			this.DetailData3_Odd.Font = new System.Drawing.Font("Tahoma", 8F);
			this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
			this.DetailData3_Odd.Name = "DetailData3_Odd";
			this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
			this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// DetailCaptionBackground3
			// 
			this.DetailCaptionBackground3.BackColor = System.Drawing.Color.Transparent;
			this.DetailCaptionBackground3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(206)))), ((int)(((byte)(206)))));
			this.DetailCaptionBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
			this.DetailCaptionBackground3.BorderWidth = 2F;
			this.DetailCaptionBackground3.Name = "DetailCaptionBackground3";
			// 
			// TotalCaption3
			// 
			this.TotalCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.TotalCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(147)))), ((int)(((byte)(147)))));
			this.TotalCaption3.Name = "TotalCaption3";
			this.TotalCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.TotalCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// TotalData3
			// 
			this.TotalData3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.TotalData3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.TotalData3.Name = "TotalData3";
			this.TotalData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 6, 0, 0, 100F);
			this.TotalData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// TotalBackground3
			// 
			this.TotalBackground3.Name = "TotalBackground3";
			// 
			// GrandTotalCaption3
			// 
			this.GrandTotalCaption3.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.GrandTotalCaption3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GrandTotalCaption3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
			this.GrandTotalCaption3.Name = "GrandTotalCaption3";
			this.GrandTotalCaption3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
			this.GrandTotalCaption3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// GrandTotalData3
			// 
			this.GrandTotalData3.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.GrandTotalData3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.GrandTotalData3.ForeColor = System.Drawing.Color.White;
			this.GrandTotalData3.Name = "GrandTotalData3";
			this.GrandTotalData3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 6, 0, 0, 100F);
			this.GrandTotalData3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// GrandTotalBackground3
			// 
			this.GrandTotalBackground3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(111)))), ((int)(((byte)(111)))));
			this.GrandTotalBackground3.BorderColor = System.Drawing.Color.White;
			this.GrandTotalBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
			this.GrandTotalBackground3.BorderWidth = 2F;
			this.GrandTotalBackground3.Name = "GrandTotalBackground3";
			// 
			// PageInfo
			// 
			this.PageInfo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
			this.PageInfo.Name = "PageInfo";
			this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPanel1,
            this.xrTable4,
            this.xrLabel5,
            this.xrPictureBox1});
			this.PageHeader.HeightF = 353.2018F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrTable4
			// 
			this.xrTable4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(10.0002F, 207.8911F);
			this.xrTable4.Name = "xrTable4";
			this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7});
			this.xrTable4.SizeF = new System.Drawing.SizeF(729.9999F, 80F);
			this.xrTable4.StylePriority.UseFont = false;
			// 
			// xrTableRow4
			// 
			this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21});
			this.xrTableRow4.Name = "xrTableRow4";
			this.xrTableRow4.Weight = 0.79999999999999993D;
			// 
			// xrTableCell19
			// 
			this.xrTableCell19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell19.Name = "xrTableCell19";
			this.xrTableCell19.StylePriority.UseFont = false;
			this.xrTableCell19.StylePriority.UseTextAlignment = false;
			this.xrTableCell19.Text = "PROJECT";
			this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell19.Weight = 0.8D;
			// 
			// xrTableCell20
			// 
			this.xrTableCell20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell20.Name = "xrTableCell20";
			this.xrTableCell20.StylePriority.UseFont = false;
			this.xrTableCell20.StylePriority.UseTextAlignment = false;
			this.xrTableCell20.Text = ":";
			this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell20.Weight = 0.14999999999999991D;
			// 
			// xrTableCell21
			// 
			this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Project")});
			this.xrTableCell21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell21.Name = "xrTableCell21";
			this.xrTableCell21.StylePriority.UseFont = false;
			this.xrTableCell21.StylePriority.UseTextAlignment = false;
			this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell21.Weight = 6.3499987792968744D;
			// 
			// xrTableRow5
			// 
			this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24});
			this.xrTableRow5.Name = "xrTableRow5";
			this.xrTableRow5.Weight = 0.8D;
			// 
			// xrTableCell22
			// 
			this.xrTableCell22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell22.Name = "xrTableCell22";
			this.xrTableCell22.StylePriority.UseFont = false;
			this.xrTableCell22.StylePriority.UseTextAlignment = false;
			this.xrTableCell22.Text = "SYSTEM";
			this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell22.Weight = 0.8D;
			// 
			// xrTableCell23
			// 
			this.xrTableCell23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell23.Name = "xrTableCell23";
			this.xrTableCell23.StylePriority.UseFont = false;
			this.xrTableCell23.StylePriority.UseTextAlignment = false;
			this.xrTableCell23.Text = ":";
			this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell23.Weight = 0.14999999999999991D;
			// 
			// xrTableCell24
			// 
			this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.System")});
			this.xrTableCell24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell24.Name = "xrTableCell24";
			this.xrTableCell24.StylePriority.UseFont = false;
			this.xrTableCell24.StylePriority.UseTextAlignment = false;
			this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell24.Weight = 6.3499987792968744D;
			// 
			// xrTableRow6
			// 
			this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27});
			this.xrTableRow6.Name = "xrTableRow6";
			this.xrTableRow6.Weight = 0.8D;
			// 
			// xrTableCell25
			// 
			this.xrTableCell25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell25.Name = "xrTableCell25";
			this.xrTableCell25.StylePriority.UseFont = false;
			this.xrTableCell25.StylePriority.UseTextAlignment = false;
			this.xrTableCell25.Text = "CUSTOMER";
			this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell25.Weight = 0.8D;
			// 
			// xrTableCell26
			// 
			this.xrTableCell26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell26.Name = "xrTableCell26";
			this.xrTableCell26.StylePriority.UseFont = false;
			this.xrTableCell26.StylePriority.UseTextAlignment = false;
			this.xrTableCell26.Text = ":";
			this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell26.Weight = 0.14999999999999991D;
			// 
			// xrTableCell27
			// 
			this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Customer")});
			this.xrTableCell27.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell27.Name = "xrTableCell27";
			this.xrTableCell27.StylePriority.UseFont = false;
			this.xrTableCell27.StylePriority.UseTextAlignment = false;
			this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell27.Weight = 6.3499987792968744D;
			// 
			// xrTableRow7
			// 
			this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30});
			this.xrTableRow7.Name = "xrTableRow7";
			this.xrTableRow7.Weight = 0.79999999999999982D;
			// 
			// xrTableCell28
			// 
			this.xrTableCell28.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell28.Name = "xrTableCell28";
			this.xrTableCell28.StylePriority.UseFont = false;
			this.xrTableCell28.StylePriority.UseTextAlignment = false;
			this.xrTableCell28.Text = "DATE";
			this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell28.Weight = 0.8D;
			// 
			// xrTableCell29
			// 
			this.xrTableCell29.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell29.Name = "xrTableCell29";
			this.xrTableCell29.StylePriority.UseFont = false;
			this.xrTableCell29.StylePriority.UseTextAlignment = false;
			this.xrTableCell29.Text = ":";
			this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell29.Weight = 0.14999999999999991D;
			// 
			// xrTableCell30
			// 
			this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubmittalReport.Date")});
			this.xrTableCell30.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrTableCell30.Name = "xrTableCell30";
			this.xrTableCell30.StylePriority.UseFont = false;
			this.xrTableCell30.StylePriority.UseTextAlignment = false;
			this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			this.xrTableCell30.Weight = 6.3499987792968744D;
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.0002F, 149.1303F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(730F, 30F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "SUBMITTAL :   [SubmittalNumber]";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrPictureBox1
			// 
			this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.xrPictureBox1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Image", null, "CompanyInfo.ReportLogo")});
			this.xrPictureBox1.FormattingRules.Add(this.UseBlankReport);
			this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(10.0002F, 10.00001F);
			this.xrPictureBox1.Name = "xrPictureBox1";
			this.xrPictureBox1.SizeF = new System.Drawing.SizeF(290.4165F, 89.66665F);
			this.xrPictureBox1.StylePriority.UseBorders = false;
			// 
			// UseBlankReport
			// 
			this.UseBlankReport.Condition = "[Parameters.printBlankReport]==True";
			this.UseBlankReport.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
			this.UseBlankReport.Name = "UseBlankReport";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel2});
			this.PageFooter.HeightF = 18F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrLabel1
			// 
			this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CompanyInfo.Name")});
			this.xrLabel1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.Red;
			this.xrLabel1.FormattingRules.Add(this.UseBlankReport);
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(206.154F, 18F);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel2
			// 
			this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CompanyInfo.Info")});
			this.xrLabel2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.ForeColor = System.Drawing.Color.Gray;
			this.xrLabel2.FormattingRules.Add(this.UseBlankReport);
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(206.154F, 0F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(543.846F, 18F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseForeColor = false;
			this.xrLabel2.StylePriority.UseTextAlignment = false;
			this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			this.xrLabel2.WordWrap = false;
			// 
			// quoteNo
			// 
			this.quoteNo.Name = "quoteNo";
			this.quoteNo.Type = typeof(int);
			this.quoteNo.ValueInfo = "11415";
			// 
			// printBlankReport
			// 
			this.printBlankReport.Name = "printBlankReport";
			this.printBlankReport.Type = typeof(bool);
			this.printBlankReport.ValueInfo = "False";
			// 
			// printQtyField
			// 
			this.printQtyField.Name = "printQtyField";
			this.printQtyField.Type = typeof(bool);
			this.printQtyField.ValueInfo = "True";
			// 
			// printLaborTimeField
			// 
			this.printLaborTimeField.Name = "printLaborTimeField";
			this.printLaborTimeField.Type = typeof(bool);
			this.printLaborTimeField.ValueInfo = "True";
			// 
			// SubmittalReport
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.groupHeaderBand1,
            this.groupHeaderBand2,
            this.groupFooterBand1,
            this.groupFooterBand2,
            this.reportFooterBand1,
            this.PageHeader,
            this.PageFooter});
			this.Bookmark = "Submittal Report";
			this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.dataSource});
			this.DataMember = "SubmittalReport";
			this.DataSource = this.dataSource;
			this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.UseBlankReport,
            this.HideQty,
            this.HideLaborTime});
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 25, 25);
			this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.quoteNo,
            this.printBlankReport,
            this.printQtyField,
            this.printLaborTimeField});
			this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.GroupCaption3,
            this.GroupData3,
            this.DetailCaption3,
            this.DetailData3,
            this.DetailData3_Odd,
            this.DetailCaptionBackground3,
            this.TotalCaption3,
            this.TotalData3,
            this.TotalBackground3,
            this.GrandTotalCaption3,
            this.GrandTotalData3,
            this.GrandTotalBackground3,
            this.PageInfo});
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRTable xrTable3;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
		private DevExpress.DataAccess.Sql.SqlDataSource dataSource;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.GroupHeaderBand groupHeaderBand1;
		private DevExpress.XtraReports.UI.GroupHeaderBand groupHeaderBand2;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRTable xrTable2;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
		private DevExpress.XtraReports.UI.GroupFooterBand groupFooterBand1;
		private DevExpress.XtraReports.UI.GroupFooterBand groupFooterBand2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.ReportFooterBand reportFooterBand1;
		private DevExpress.XtraReports.UI.XRControlStyle Title;
		private DevExpress.XtraReports.UI.XRControlStyle GroupCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle GroupData3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailData3;
		private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
		private DevExpress.XtraReports.UI.XRControlStyle DetailCaptionBackground3;
		private DevExpress.XtraReports.UI.XRControlStyle TotalCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle TotalData3;
		private DevExpress.XtraReports.UI.XRControlStyle TotalBackground3;
		private DevExpress.XtraReports.UI.XRControlStyle GrandTotalCaption3;
		private DevExpress.XtraReports.UI.XRControlStyle GrandTotalData3;
		private DevExpress.XtraReports.UI.XRControlStyle GrandTotalBackground3;
		private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRTable xrTable4;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
		private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
		private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.Parameters.Parameter quoteNo;
		private DevExpress.XtraReports.Parameters.Parameter printBlankReport;
		private DevExpress.XtraReports.Parameters.Parameter printQtyField;
		private DevExpress.XtraReports.Parameters.Parameter printLaborTimeField;
		private DevExpress.XtraReports.UI.FormattingRule HideQty;
		private DevExpress.XtraReports.UI.FormattingRule HideLaborTime;
		private DevExpress.XtraReports.UI.FormattingRule UseBlankReport;
		private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
	}
}
