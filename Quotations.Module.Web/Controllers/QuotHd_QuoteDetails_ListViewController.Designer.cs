﻿namespace Quotations.Module.Web.Controllers
{
	partial class QuotHd_QuoteDetails_ListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SearchItems = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.RefreshPrices = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// SearchItems
			// 
			this.SearchItems.AcceptButtonCaption = null;
			this.SearchItems.CancelButtonCaption = null;
			this.SearchItems.Caption = "Search Items";
			this.SearchItems.Category = "Edit";
			this.SearchItems.ConfirmationMessage = null;
			this.SearchItems.Id = "SearchItems";
			this.SearchItems.ToolTip = null;
			this.SearchItems.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SearchItems_CustomizePopupWindowParams);
			this.SearchItems.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.SearchItems_Execute);
			// 
			// RefreshPrices
			// 
			this.RefreshPrices.Caption = "Refresh Prices";
			this.RefreshPrices.Category = "Edit";
			this.RefreshPrices.ConfirmationMessage = null;
			this.RefreshPrices.Id = "RefreshPrices";
			this.RefreshPrices.ToolTip = null;
			this.RefreshPrices.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RefreshPrices_Execute);
			// 
			// QuotHd_QuoteDetails_ListViewController
			// 
			this.Actions.Add(this.SearchItems);
			this.Actions.Add(this.RefreshPrices);
			this.TargetObjectType = typeof(Quotations.Module.BusinessObjects.Quotations.QuotDtl);
			this.TargetViewId = "QuotHd_QuoteDetails_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.PopupWindowShowAction SearchItems;
		private DevExpress.ExpressApp.Actions.SimpleAction RefreshPrices;
	}
}
