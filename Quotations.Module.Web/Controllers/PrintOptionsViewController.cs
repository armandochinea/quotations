﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class PrintOptionsViewController : ObjectViewController
	{
		public PrintOptionsViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		private ASPxEnumPropertyEditor quotationEnumPropertyEditor;
		private ASPxEnumPropertyEditor submittalEnumPropertyEditor;

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			quotationEnumPropertyEditor = View.FindItem("QuotationPrintOptions") as ASPxEnumPropertyEditor;
			if (quotationEnumPropertyEditor != null)
			{
				quotationEnumPropertyEditor.ControlCreated += EnumPropertyEditor_ControlCreated;
			}

			submittalEnumPropertyEditor = View.FindItem("SubmittalPrintOptions") as ASPxEnumPropertyEditor;
			if (submittalEnumPropertyEditor != null)
			{
				submittalEnumPropertyEditor.ControlCreated += EnumPropertyEditor_ControlCreated;
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			if (quotationEnumPropertyEditor != null)
			{
				quotationEnumPropertyEditor.ControlCreated -= EnumPropertyEditor_ControlCreated;
				quotationEnumPropertyEditor = null;
			}

			if (submittalEnumPropertyEditor != null)
			{
				submittalEnumPropertyEditor.ControlCreated -= EnumPropertyEditor_ControlCreated;
				submittalEnumPropertyEditor = null;
			}
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void EnumPropertyEditor_ControlCreated(object sender, EventArgs e)
		{
			if (quotationEnumPropertyEditor != null)
			{
				if (quotationEnumPropertyEditor.Editor != null)
				{
					quotationEnumPropertyEditor.Editor.Init += Editor_Init;
				}
			}

			if (submittalEnumPropertyEditor != null)
			{
				if (submittalEnumPropertyEditor.Editor != null)
				{
					submittalEnumPropertyEditor.Editor.Init += Editor_Init;
				}
			}
		}

		private void Editor_Init(object sender, EventArgs e)
		{
			try
			{
				var user = View.ObjectSpace.GetObjectsQuery<SecuritySystemUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

				if (user != null)
				{
					if (quotationEnumPropertyEditor != null)
					{
						ASPxComboBox comboBox = quotationEnumPropertyEditor.Editor;

						if (!user.Roles.Any(r => r.Name == "Administrators"))
						{
							if (!user.Roles.Any(r => r.Name == "Quotation Report (Print Cost & Labor)"))
							{
								ListEditItem item = comboBox.Items.FindByValue(Enums.QuotationPrintOptions.CostLabor);
								comboBox.Items.Remove(item);
							}

							if (!user.Roles.Any(r => r.Name == "Quotation Report (Print Item Price)"))
							{
								ListEditItem item = comboBox.Items.FindByValue(Enums.QuotationPrintOptions.ItemPrice);
								comboBox.Items.Remove(item);
							}
						}
					}

					if (submittalEnumPropertyEditor != null)
					{
						ASPxComboBox comboBox = submittalEnumPropertyEditor.Editor;

						if (!user.Roles.Any(r => r.Name == "Administrators"))
						{
							if (!user.Roles.Any(r => r.Name == "Submittal Report (Print Labor Time)"))
							{
								ListEditItem item = comboBox.Items.FindByValue(Enums.SubmittalPrintOptions.LaborTime);
								comboBox.Items.Remove(item);
							}

							if (!user.Roles.Any(r => r.Name == "Submittal Report (Print Quantity & Labor Time)"))
							{
								ListEditItem item = comboBox.Items.FindByValue(Enums.SubmittalPrintOptions.QtyLabor);
								comboBox.Items.Remove(item);
							}

							if (!user.Roles.Any(r => r.Name == "Submittal Report (Print Quantity)"))
							{
								ListEditItem item = comboBox.Items.FindByValue(Enums.SubmittalPrintOptions.Quantity);
								comboBox.Items.Remove(item);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("Error message: {0}, Stack Trace: {1}", ex.Message ?? string.Empty, ex.StackTrace ?? string.Empty));
			}
		}
	}
}
