﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ReportsV2;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.ExpressApp.ReportsV2.Web;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class ReportsViewController : ViewController
	{
		//ReportDataSourceHelper helper;

		public ReportsViewController ()
		{
			TargetViewId = ReportsAspNetModuleV2.ReportViewDetailViewWebName;
		}

		protected override void OnActivated()
		{
			base.OnActivated();
			//helper = Application.Modules.FindModule<ReportsModuleV2>().ReportsDataSourceHelper;
			//if (helper != null) helper.BeforeShowPreview += ReportsDataSourceHelper_BeforeShowPreview;

			//if (!View.IsDisposed && View.Id == "ReportViewer_DetailView_V2")
			//{

			//}
			//ReportWebViewerDetailItem reportViewItem = ((DetailView)View).GetItems<ReportWebViewerDetailItem>()[0] as ReportWebViewerDetailItem;
			//reportViewItem.ControlCreated += delegate (object sender, EventArgs e) {
			//	// Access client-side events of the ASPxWebDocumentViewer control 
			//	reportViewItem.ReportViewer.ClientSideEvents.Init =
			//		"function(s, e) { s.previewModel.reportPreview.zoom(0.7); }";
			//};
		}
		void ReportsDataSourceHelper_BeforeShowPreview(object sender, BeforeShowPreviewEventArgs e)
		{
			if (e.Report.DisplayName == "QuotationReport" || e.Report.DisplayName == "QuotationCostLaborReport" || e.Report.DisplayName == "QuotationItemPriceReport" || e.Report.DisplayName == "SubmittalReport")
			{
				var currentViewObject = View.CurrentObject as QuotHd;
				if (currentViewObject != null)
				{
					e.Report.Parameters["quotNo"].Value = currentViewObject.ID;
					e.Report.Parameters["printBlankReport"].Value = false;

					if (e.Report.DisplayName == "SubmittalReport")
					{
						e.Report.Parameters["printLaborTimeField"].Value = true;
						e.Report.Parameters["printQtyField"].Value = true;
					}
				}
			}
		}
		protected override void OnDeactivated()
		{
			//if (helper != null) helper.BeforeShowPreview -= ReportsDataSourceHelper_BeforeShowPreview;
		}
	}
}
