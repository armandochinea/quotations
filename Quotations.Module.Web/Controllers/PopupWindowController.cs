﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web;
using System.Web.UI.WebControls;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class PopupWindowController : ViewController
	{
		public PopupWindowController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			((WebApplication)Application).PopupWindowManager.PopupShowing += PopupWindowManager_PopupShowing;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			((WebApplication)Application).PopupWindowManager.PopupShowing -= PopupWindowManager_PopupShowing;

			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void PopupWindowManager_PopupShowing(object sender, PopupShowingEventArgs e)
		{
			e.PopupControl.CustomizePopupWindowSize += XafPopupWindowControl_CustomizePopupWindowSize;
		}

		private void XafPopupWindowControl_CustomizePopupWindowSize(object sender, DevExpress.ExpressApp.Web.Controls.CustomizePopupWindowSizeEventArgs e)
		{
			Unit width = new Unit(700);
			Unit height = new Unit(500);

			if (e.PopupFrame.View.ObjectTypeInfo.Type == typeof(ItemSearch))
			{
				width = new Unit(1000);
				height = new Unit(850);
			}

			if (e.PopupFrame.View.ObjectTypeInfo.Type == typeof(ReportDataV2))
			{
				width = new Unit(1500);
				height = new Unit(900);
			}

			if (e.PopupFrame.View.ObjectTypeInfo.Type == typeof(PrintOptions))
			{
				width = new Unit(450);
				height = new Unit(400);
			}

			e.Width = width;
			e.Height = height;
			e.Handled = true;
		}
	}
}
