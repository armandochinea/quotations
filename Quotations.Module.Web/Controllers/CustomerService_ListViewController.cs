﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CustomerService_ListViewController : ViewController
	{
		public CustomerService_ListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		Project selectedProject = null;

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CustomerService_ListViewController_CustomProcessSelectedItem;

			ListViewController listViewController = Frame.GetController<ListViewController>();
			if (listViewController != null)
			{
				listViewController.EditAction.Active["EditActive"] = false;
			}
		}

		private void CustomerService_ListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;

			if (!View.IsDisposed)
			{
				selectedProject = (Project)View.CurrentObject;

				if (selectedProject != null)
				{
					IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(Project));
					DetailView detailView = Application.CreateDetailView(newObjSpace, "CustomerService_DetailView", true, newObjSpace.GetObject(selectedProject));
					if (detailView != null)
					{
						var user = View.ObjectSpace.GetObjectsQuery<SecuritySystemUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
						if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit Projects" || r.Name == "Customer Service"))
						{
							detailView.ViewEditMode = ViewEditMode.Edit;
						}
							
						Frame.SetView(detailView);
					}
				}
			}
		}

		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= CustomerService_ListViewController_CustomProcessSelectedItem;

			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}
	}
}
