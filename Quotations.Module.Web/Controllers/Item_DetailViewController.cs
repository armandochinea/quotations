﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Web.SystemModule;
using System.ComponentModel;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class Item_DetailViewController : ViewController
	{
		public Item_DetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			try
			{
				if (!View.IsDisposed && View.Id == "Item_DetailView")
				{
					var user = View.ObjectSpace.GetObjectsQuery<SecuritySystemUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

					if (user != null)
					{
						WebModificationsController controller = Frame.GetController<WebModificationsController>();
						if (controller != null)
						{
							if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit Items"))
							{
								controller.EditAction.Active["EditActive"] = true;
							}
							else
							{
								controller.EditAction.Active["EditActive"] = false;
							}
						}
					}

					Application.ObjectSpaceCreated += Application_ObjectSpaceCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("Error message: {0}, Stack Trace: {1}", ex.Message ?? string.Empty, ex.StackTrace ?? string.Empty));
			}
			
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
			Application.ObjectSpaceCreated -= Application_ObjectSpaceCreated;
		}
		void Application_ObjectSpaceCreated(object sender, ObjectSpaceCreatedEventArgs e)
		{
			e.ObjectSpace.Committing += ObjectSpace_Committing;
		}
		void ObjectSpace_Committing(object sender, CancelEventArgs e)
		{
			IObjectSpace os = (IObjectSpace)sender;
			if (os.ModifiedObjects.Count > 0)
			{
				Tracing.Tracer.LogText("ObjectSpace_Committing");
				Tracing.Tracer.LogText("Current User - " + SecuritySystem.CurrentUserName);
				View currentView = os.Owner as View;
				if (currentView != null)
				{
					Tracing.Tracer.LogText("Current View - " + currentView.Id);
					Tracing.Tracer.LogText("Current Object - " + (currentView.CurrentObject == null ? "null" : os.GetObjectHandle(currentView.CurrentObject)));
				}
				Tracing.Tracer.LogText("Modified Objects:");
				foreach (Object obj in os.ModifiedObjects)
				{
					Tracing.Tracer.LogText(os.GetObjectHandle(obj));
				}
				Tracing.Tracer.LogText(Environment.StackTrace);
			}
		}
	}
}
