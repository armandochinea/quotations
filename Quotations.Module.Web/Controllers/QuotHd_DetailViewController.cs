﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.ExpressApp.Web;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraReports.UI;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Web.SystemModule;
using System.Web.UI;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web.Editors.ASPx;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class QuotHd_DetailViewController : ViewController
	{
		public QuotHd_DetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		bool doFirstCommit = true;

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			try
			{
				if (!View.IsDisposed && View.Id == "QuotHd_DetailView")
				{
					var detailView = View as DetailView;
					if (detailView != null)
					{
						if (detailView.ViewEditMode == ViewEditMode.Edit)
						{
							if (doFirstCommit)
							{
								doFirstCommit = false;
								View.ObjectSpace.CommitChanges();
							}
						}
						else
						{
							doFirstCommit = true;

							var curObj = ((DetailView)View.ObjectSpace.Owner).CurrentObject as QuotHd;
							var user = View.ObjectSpace.GetObjectsQuery<BtcUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (user != null)
							{
								if (curObj != null)
								{
									QuotPrt quotPrt = View.ObjectSpace.GetObjectsQuery<QuotPrt>().Where(r => r.QuotNo.ID == curObj.ID).FirstOrDefault();
									if (quotPrt == null)
									{
										quotPrt = View.ObjectSpace.CreateObject<QuotPrt>();
										quotPrt.QuotNo = View.ObjectSpace.GetObjectByKey<QuotHd>(curObj.ID);
										quotPrt.Accept = string.Empty;
										quotPrt.By = user.FullName;
									}
								}

								WebModificationsController controller = Frame.GetController<WebModificationsController>();
								if (controller != null)
								{
									if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit Quotations"))
									{
										controller.EditAction.Active["EditActive"] = true;
									}
									else
									{
										controller.EditAction.Active["EditActive"] = false;
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.

			try
			{
				if (!View.IsDisposed && View.Id == "QuotHd_DetailView")
				{
					var detailView = (View as DetailView);

					if (detailView != null)
					{
						bool isEditMode = detailView.ViewEditMode == ViewEditMode.Edit;

                        var user = View.ObjectSpace.GetObjectsQuery<BtcUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

                        if (user != null)
                        {
                            // Set the visibility of the Search Items and Refresh Prices buttons if view is in EditMode.
                            ListPropertyEditor listPropertyEditor = detailView.FindItem("QuotDtls") as ListPropertyEditor;
                            if (listPropertyEditor != null)
                            {
                                var viewController = listPropertyEditor.Frame.GetController<QuotHd_QuoteDetails_ListViewController>();
                                if (viewController != null)
                                {
                                    viewController.Actions["SearchItems"].Active["IsActive"] = isEditMode;
                                }

                                if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit Quotations"))
                                {
                                    viewController.Actions["RefreshPrices"].Active["IsActive"] = !isEditMode;
                                }
                                else
                                {
                                    viewController.Actions["RefreshPrices"].Active["IsActive"] = false;
                                }
                            }

                            // Only users with the Administrators or Edit BTC Number for Quotations roles can update the Project (BTC Number) of a Quote. 
                            if ((detailView.CurrentObject as QuotHd).Project != null)
                            {
                                ASPxLookupPropertyEditor projectPropertyEditor = detailView.FindItem("Project") as ASPxLookupPropertyEditor;
                                if (projectPropertyEditor != null)
                                {
                                    if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit BTC Number for Quotations"))
                                    {
                                        projectPropertyEditor.AllowEdit["ReassignProject"] = true;
                                    }
                                    else
                                    {
                                        projectPropertyEditor.AllowEdit["ReassignProject"] = false;
                                    }
                                }
                            }

                            // Only administrators can print a Quotation even if it was already assigned to a Project.
                            if (user.Roles.Any(r => r.Name == "Administrators"))
                            {
                                PrintQuotation.TargetObjectsCriteria = "";
                            }
                            else
                            {
                                PrintQuotation.TargetObjectsCriteria = "[Project] Is Null";
                            }
                        }
                    }
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}
		protected override void OnDeactivated()
		{
			try
			{
				if (!View.IsDisposed && View.Id == "QuotHd_DetailView")
				{
					doFirstCommit = true;
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
			
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void HandleException (Exception ex)
		{
			throw new Exception(string.Format("Error message: {0}, Stack Trace: {1}", ex.Message ?? string.Empty, ex.StackTrace ?? string.Empty));
		}

		private void QuotContinue_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				var quotDetailView = (View as DetailView);

				if (quotDetailView != null)
				{
					if (quotDetailView.ViewEditMode == ViewEditMode.Edit)
					{
						quotDetailView.ObjectSpace.CommitChanges();
					}
				}

				IObjectSpace objSpace = Application.CreateObjectSpace(typeof(QuotHd));
				var curObj = objSpace.GetObject<QuotHd>((View as DetailView).CurrentObject as QuotHd);
				bool changesDone = false;

				if (quotDetailView.ViewEditMode == ViewEditMode.Edit)
				{
					if (!curObj.IsLegacy && curObj.TaxPercent != 0 && curObj.Tax == 0)
					{
						// This is done to force calculation of the sales tax.
						var temp = curObj.TaxPercent;
						curObj.TaxPercent = 0;
						curObj.TaxPercent = temp;
						changesDone = true;
					}

					if (!curObj.IsLegacy && curObj.SalesCommissionPercent != 0 && curObj.SalesCommission == 0)
					{
						// This is done to force calculation of the sales tax.
						var temp = curObj.SalesCommissionPercent;
						curObj.SalesCommissionPercent = 0;
						curObj.SalesCommissionPercent = temp;
						changesDone = true;
					}

					if (changesDone)
					{
						objSpace.CommitChanges();
					}
				}

				var viewId = curObj.IsLegacy ? "QuotHd_OtherCharges_DetailView_Legacy" : "QuotHd_OtherCharges_DetailView";
				var otherCostDetailView = Application.CreateDetailView(objSpace, viewId, false, curObj);
				otherCostDetailView.ViewEditMode = quotDetailView.ViewEditMode;

				if (otherCostDetailView.ViewEditMode == ViewEditMode.View)
				{
					e.DialogController.AcceptAction.Active["IsActive"] = false;
					e.DialogController.CancelAction.Caption = "Close";
				}
				else
				{
					e.DialogController.AcceptAction.Active["IsActive"] = true;
					e.DialogController.CancelAction.Caption = "Cancel";
					e.DialogController.Accepting += DialogController_Accepting;
					e.DialogController.SaveOnAccept = true;
				}

				e.View = otherCostDetailView;
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void DialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			try
			{
				DialogController dialogController = (DialogController)sender;
				dialogController.Window.View.ObjectSpace.CommitChanges();
				View.ObjectSpace.Refresh();
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void QuotContinue_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
		{
			try
			{
				View.ObjectSpace.CommitChanges();

				NonPersistentObjectSpace printOptObjSpace = (NonPersistentObjectSpace)Application.CreateObjectSpace(typeof(PrintOptions));

				if (printOptObjSpace != null)
				{
					printOptObjSpace.AdditionalObjectSpaces.Add(Application.CreateObjectSpace(typeof(QuotPrt)));
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void PrintSubmittal_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				NonPersistentObjectSpace printOptObjSpace = (NonPersistentObjectSpace)Application.CreateObjectSpace(typeof(PrintOptions));

				if (printOptObjSpace != null)
				{
					IObjectSpace quotPrtObjSpace = Application.CreateObjectSpace(typeof(QuotPrt));
					printOptObjSpace.AdditionalObjectSpaces.Add(quotPrtObjSpace);

					var quotation = (View.ObjectSpace.Owner as DetailView).CurrentObject as QuotHd;

					PrintOptions printOptions = (PrintOptions)printOptObjSpace.CreateObject(typeof(PrintOptions));
					if (printOptions != null)
					{
						printOptions.ID = 0;
						printOptions.ReportType = Enums.ReportType.Submittal;
						printOptions.SubmittalPrintOptions = Enums.SubmittalPrintOptions.None;
						printOptions.QuotNo = quotation.ID;

						QuotPrt quotPrt = quotPrtObjSpace.GetObjectByKey<QuotPrt>(quotation.ID);
						if (quotPrt == null)
						{
							quotPrt = quotPrtObjSpace.CreateObject<QuotPrt>();
							quotPrt.QuotNo = quotPrtObjSpace.GetObjectByKey<QuotHd>(quotation.ID);
							quotPrt.Accept = string.Empty;
							quotPrt.By = quotPrtObjSpace.GetObjectsQuery<BtcUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault().FullName;
						}
						printOptions.QuotPrt = printOptObjSpace.GetObject<QuotPrt>(quotPrt);

						DetailView createdView = Application.CreateDetailView(printOptObjSpace, "PrintOptions_Submittal_DetailView", false, printOptions);
						createdView.ViewEditMode = ViewEditMode.Edit;
						e.View = createdView;
					}
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void PrintSubmittal_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
		{
			try
			{
				string reportName = "";

				/*
				Submittal (Blank/Qty & Labor)
				Submittal (Blank/Labor)
				Submittal (Blank/Qty)
				Submittal (Pre-Printed/Qty)
				Submittal (Pre-Printed)
				Submittal (Pre-Printed/Qty & Labor)
				Submittal (Pre-Printed/Labor)
				Submittal (Blank) 
				*/

				PrintOptions printOptions = (PrintOptions)e.PopupWindowViewCurrentObject;

				if (printOptions != null)
				{
					if (printOptions.FormType == Enums.FormType.Blank)
					{
						switch (printOptions.SubmittalPrintOptions)
						{
							case Enums.SubmittalPrintOptions.None:
								reportName = "Submittal (Blank)";
								break;
							case Enums.SubmittalPrintOptions.Quantity:
								reportName = "Submittal (Blank/Qty)";
								break;
							case Enums.SubmittalPrintOptions.LaborTime:
								reportName = "Submittal (Blank/Labor)";
								break;
							case Enums.SubmittalPrintOptions.QtyLabor:
								reportName = "Submittal (Blank/Qty & Labor)";
								break;
							default:
								break;
						}
					}
					else
					{
						switch (printOptions.SubmittalPrintOptions)
						{
							case Enums.SubmittalPrintOptions.None:
								reportName = "Submittal (Pre-Printed)";
								break;
							case Enums.SubmittalPrintOptions.Quantity:
								reportName = "Submittal (Pre-Printed/Qty)";
								break;
							case Enums.SubmittalPrintOptions.LaborTime:
								reportName = "Submittal (Pre-Printed/Labor)";
								break;
							case Enums.SubmittalPrintOptions.QtyLabor:
								reportName = "Submittal (Pre-Printed/Qty & Labor)";
								break;
							default:
								break;
						}
					}
				}
				IObjectSpace objectSpace = ReportDataProvider.ReportObjectSpaceProvider.CreateObjectSpace(typeof(ReportDataV2));
				IReportDataV2 reportData = objectSpace.FindObject<ReportDataV2>(CriteriaOperator.Parse("[DisplayName] = ?", reportName));

				string handle = ReportDataProvider.ReportsStorage.GetReportContainerHandle(reportData);
				MyWebReportServiceController controller = Frame.GetController<MyWebReportServiceController>();
				if (controller != null)
				{
					var curQuote = (View.ObjectSpace.Owner as DetailView).CurrentObject as QuotHd;
					controller.ShowPreview(handle, null, CriteriaOperator.Parse("ID = ?", curQuote.ID), true, null, false, e.ShowViewParameters);
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void PrintQuotation_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				NonPersistentObjectSpace printOptObjSpace = (NonPersistentObjectSpace)Application.CreateObjectSpace(typeof(PrintOptions));

				if (printOptObjSpace != null)
				{
					IObjectSpace quotPrtObjSpace = Application.CreateObjectSpace(typeof(QuotPrt));
					printOptObjSpace.AdditionalObjectSpaces.Add(quotPrtObjSpace);

					var quotation = (View.ObjectSpace.Owner as DetailView).CurrentObject as QuotHd;

					PrintOptions printOptions = (PrintOptions)printOptObjSpace.CreateObject(typeof(PrintOptions));
					if (printOptions != null)
					{
						printOptions.ID = 0;
						printOptions.ReportType = Enums.ReportType.Quotation;
						printOptions.QuotationPrintOptions = Enums.QuotationPrintOptions.None;
						printOptions.QuotNo = quotation.ID;

						QuotPrt quotPrt = quotPrtObjSpace.GetObjectByKey<QuotPrt>(quotation.ID);
						if (quotPrt == null)
						{
							quotPrt = quotPrtObjSpace.CreateObject<QuotPrt>();
							quotPrt.QuotNo = quotPrtObjSpace.GetObjectByKey<QuotHd>(quotation.ID);
							quotPrt.Accept = string.Empty;
							quotPrt.By = quotPrtObjSpace.GetObjectsQuery<BtcUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault().FullName;
							//quotPrtObjSpace.CommitChanges();
						}
						printOptions.QuotPrt = printOptObjSpace.GetObject<QuotPrt>(quotPrt);

						DetailView createdView = Application.CreateDetailView(printOptObjSpace, "PrintOptions_Quotation_DetailView", false, printOptions);
						createdView.ViewEditMode = ViewEditMode.Edit;
						e.View = createdView;
					}
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void PrintQuotation_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
		{
			try
			{
				string reportName = "";

				/*
				Quotation (Pre-Printed/Cost & Labor)
				Quotation (Pre-Printed)
				Quotation (Blank/Item Price)
				Quotation (Blank/Cost & Labor)
				Quotation (Blank)
				Quotation (Pre-Printed/Item Price)
				*/

				PrintOptions printOptions = (PrintOptions)e.PopupWindowViewCurrentObject;

				if (printOptions != null)
				{
					NonPersistentObjectSpace popupObjectSpace = (NonPersistentObjectSpace)e.PopupWindowView.ObjectSpace;
					IObjectSpace quotPrtObjSpace = popupObjectSpace.AdditionalObjectSpaces.First();
					printOptions.QuotPrt.DatePrt = DateTime.Now;
					quotPrtObjSpace.CommitChanges();

					if (printOptions.FormType == Enums.FormType.Blank)
					{
						switch (printOptions.QuotationPrintOptions)
						{
							case Enums.QuotationPrintOptions.None:
								reportName = "Quotation (Blank)";
								break;
							case Enums.QuotationPrintOptions.CostLabor:
								reportName = "Quotation (Blank/Cost & Labor)";
								break;
							case Enums.QuotationPrintOptions.ItemPrice:
								reportName = "Quotation (Blank/Item Price)";
								break;
							default:
								break;
						}
					}
					else
					{
						switch (printOptions.QuotationPrintOptions)
						{
							case Enums.QuotationPrintOptions.None:
								reportName = "Quotation (Pre-Printed)";
								break;
							case Enums.QuotationPrintOptions.CostLabor:
								reportName = "Quotation (Pre-Printed/Cost & Labor)";
								break;
							case Enums.QuotationPrintOptions.ItemPrice:
								reportName = "Quotation (Pre-Printed/Item Price)";
								break;
							default:
								break;
						}
					}
				}
				IObjectSpace objectSpace = ReportDataProvider.ReportObjectSpaceProvider.CreateObjectSpace(typeof(ReportDataV2));
				IReportDataV2 reportData = objectSpace.FindObject<ReportDataV2>(CriteriaOperator.Parse("[DisplayName] = ?", reportName));

				string handle = ReportDataProvider.ReportsStorage.GetReportContainerHandle(reportData);
				MyWebReportServiceController controller = Frame.GetController<MyWebReportServiceController>();
				if (controller != null)
				{
					var curQuote = (View.ObjectSpace.Owner as DetailView).CurrentObject as QuotHd;
					controller.ShowPreview(handle, null, CriteriaOperator.Parse("ID = ?", curQuote.ID), true, null, false, e.ShowViewParameters);
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}	
	}
}
