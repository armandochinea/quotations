﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Quotations.Module.BusinessObjects.Quotations;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.ExpressApp.Security.Strategy;
using System.Collections;
using DevExpress.Xpo.Helpers;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class QuotHd_QuoteDetails_ListViewController : ViewController
	{
		public QuotHd_QuoteDetails_ListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			((ListView)View).Editor.ControlsCreated += new EventHandler(Editor_ControlsCreated);
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void HandleException(Exception ex)
		{
			throw new Exception(string.Format("Error message: {0}, Stack Trace: {1}", ex.Message ?? string.Empty, ex.StackTrace ?? string.Empty));
		}

		void Editor_ControlsCreated(object sender, EventArgs e)
		{
			ASPxGridView gridView = ((ListEditor)sender).Control as ASPxGridView;
			gridView.DataBound += new EventHandler(gridView_DataBound);
			gridView.Load += new EventHandler(gridView_Load);
		}

		void gridView_Load(object sender, EventArgs e)
		{
			UpdateColumnVisible((ASPxGridView)sender);
		}

		void gridView_DataBound(object sender, EventArgs e)
		{
			UpdateColumnVisible((ASPxGridView)sender);
		}

		private void UpdateColumnVisible(ASPxGridView gridView)
		{
			try
			{
				// Hide Price and Ext. Price columns from the grid if the user has the [Hide Quotation Prices] role assigned.
				var user = View.ObjectSpace.GetObjectsQuery<SecuritySystemUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
				if (user != null)
				{
					bool visible = !user.Roles.Any(r => r.Name == "Hide Quotation Prices");
					foreach (GridViewColumn col in gridView.Columns)
					{
						if (col.Caption == "Price" || col.Caption == "Ext. Price")
						{
							col.Visible = visible;
						}
					}
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void SearchItems_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				var mDetailView = View.ObjectSpace.Owner as DetailView;
				var curObj = mDetailView.CurrentObject;
				var curUser = SecuritySystem.CurrentUserName;

				Type objectType = typeof(ItemSearch);
				IObjectSpace objSpace = Application.CreateObjectSpace(objectType);

				QuotHd quotHd = (curObj as QuotHd);

				if (quotHd != null)
				{
					string sql = "";

					sql = string.Format("DELETE FROM ItemSearch WHERE [User] = '{0}';", curUser);
					sql += string.Format("INSERT INTO ItemSearch SELECT ID = NEWID(), [User] = '{0}', Item = ItemNo, Qty = 0 from Item where ItemActCode = 1;", curUser);

					foreach (var qDetail in quotHd.QuotDtls)
					{
						sql += string.Format("UPDATE ItemSearch SET Qty = {0} where [User] = '{1}' and Item = '{2}';", qDetail.Qty, curUser, qDetail.Item.ID);
					}

					((XPObjectSpace)objSpace).Session.ExecuteNonQuery(sql);
				}

				string listViewId = "ItemSearch_ListView";
				CollectionSourceBase collectionSource = Application.CreateCollectionSource(objSpace, typeof(ItemSearch), listViewId);
				collectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse(string.Format("User = '{0}'", curUser));
				ListView listView = Application.CreateListView(listViewId, collectionSource, false);
				e.View = listView;

				DialogController dialogController = new DialogController();
				e.DialogController = dialogController;

				dialogController.ViewClosed += SearchItemsDialogController_ViewClosed;

				quotHd = null;
				objSpace = null;

				GC.WaitForPendingFinalizers();
				GC.Collect();
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void SearchItemsDialogController_ViewClosed(object sender, EventArgs e)
		{
			try
			{
				var dialogController = sender as DialogController;
				if (dialogController != null)
				{
					dialogController.ViewClosed -= SearchItemsDialogController_ViewClosed;
				}

				var objSpace = View.ObjectSpace;
				var curUser = SecuritySystem.CurrentUserName;

				((XPObjectSpace)objSpace).Session.ExecuteNonQuery(string.Format("DELETE FROM ItemSearch WHERE [User] = '{0}'", curUser));
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void SearchItems_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
		{
			try
			{
				ListView itemSearchListView = (e.PopupWindowView) as ListView;
				var objSpace = itemSearchListView.ObjectSpace;

				DevExpress.Xpo.Session session = ((XPObjectSpace)(objSpace)).Session;

				QuotHd quotHd = objSpace.GetObject(((QuotHd)(View.ObjectSpace.Owner as DetailView).CurrentObject));

				if (quotHd != null)
				{
					session.Delete(quotHd.QuotDtls);
					session.Save(quotHd.QuotDtls);

					itemSearchListView.CollectionSource.Criteria.Clear();
                    itemSearchListView.CollectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse(string.Format("User = '{0}'", SecuritySystem.CurrentUserName));

                    List<ItemSearch> itemSearchList = new List<ItemSearch>();
					if (itemSearchListView.CollectionSource.List.GetType() == typeof(XpoServerCollectionFlagger))
					{
						ItemSearch item = null;
						for (int i = 0; i < itemSearchListView.CollectionSource.List.Count - 1; i++)
						{
							item = (ItemSearch)itemSearchListView.CollectionSource.List[i];
							if (item.Qty > 0)
							{
								itemSearchList.Add(item);
							}
						}
						item = null;
					}
					else
					{
						foreach (ItemSearch item in itemSearchListView.CollectionSource.List)
						{
							if (item.Qty > 0)
							{
								itemSearchList.Add((ItemSearch)item);
							}
						}
					}

					foreach (ItemSearch obj in itemSearchList)
					{
						var quoteDetail = objSpace.CreateObject<QuotDtl>();
						var item = objSpace.GetObjectsQuery<Item>().Where(r => r.ID == obj.Item.ID).FirstOrDefault();
						quoteDetail.QuotHd = quotHd;
						quoteDetail.Item = item;

						decimal qty = obj.Qty;
						quoteDetail.Qty = qty;
						
						quoteDetail.Save();
						quotHd.QuotDtls.Add(quoteDetail);
						quotHd.Save();
						quoteDetail = null;
					}

					objSpace.CommitChanges();
					View.RefreshDataSource();

					itemSearchList = null;
				}

				itemSearchListView = null;
				quotHd = null;
				objSpace = null;
				session = null;
				GC.Collect();
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void RefreshPrices_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			QuotDtl line = null;
			decimal tempQty = 0;
			var listView = View as ListView;
			foreach (QuotDtl detail in (IList)listView.CollectionSource.Collection)
			{
				line = View.ObjectSpace.GetObjectsQuery<QuotDtl>().Where(r => r.ID == detail.ID).FirstOrDefault();
				if (line != null)
				{
					tempQty = line.Qty;
					line.Qty = 0;
					line.Qty = tempQty;
					line.Save();
				}
			}
			View.ObjectSpace.CommitChanges();
			(View.ObjectSpace.Owner as DetailView).Refresh();
		}
	}
}
