﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class Project_ListViewController : ViewController
	{
		public Project_ListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		ListViewController listViewController = null;
		NewObjectViewController newObjectController = null;

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			try
			{
				var user = View.ObjectSpace.GetObjectsQuery<SecuritySystemUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

				if (user != null)
				{
					listViewController = Frame.GetController<ListViewController>();
					if (listViewController != null)
					{
						if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Edit Projects"))
						{
							listViewController.EditAction.Active["EditActive"] = true;
						}
						else
						{
							listViewController.EditAction.Active["EditActive"] = false;
						}
					}

					newObjectController = Frame.GetController<NewObjectViewController>();
					if (newObjectController != null)
					{
						if (user.Roles.Any(r => r.Name == "Administrators" || r.Name == "Create Projects"))
						{
							newObjectController.NewObjectAction.Active["NewActive"] = true;
						}
						else
						{
							newObjectController.NewObjectAction.Active["NewActive"] = false;
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("Error message: {0}, Stack Trace: {1}", ex.Message ?? string.Empty, ex.StackTrace ?? string.Empty));
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			if (listViewController != null)
			{
				listViewController.EditAction.Active["EditActive"] = true;
				listViewController = null;
			}

			if (newObjectController != null)
			{
				newObjectController.NewObjectAction.Active["NewActive"] = true;
				newObjectController = null;
			}
		}
	}
}
