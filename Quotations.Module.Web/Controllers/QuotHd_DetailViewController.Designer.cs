﻿namespace Quotations.Module.Web.Controllers
{
	partial class QuotHd_DetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.QuotContinue = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.PrintSubmittal = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.PrintQuotation = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			// 
			// QuotContinue
			// 
			this.QuotContinue.AcceptButtonCaption = "Accept";
			this.QuotContinue.CancelButtonCaption = null;
			this.QuotContinue.Caption = "Continue";
			this.QuotContinue.Category = "QuotContinue";
			this.QuotContinue.ConfirmationMessage = null;
			this.QuotContinue.Id = "QuotContinue";
			this.QuotContinue.TargetObjectType = typeof(Quotations.Module.BusinessObjects.Quotations.QuotHd);
			this.QuotContinue.ToolTip = null;
			this.QuotContinue.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.QuotContinue_CustomizePopupWindowParams);
			this.QuotContinue.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.QuotContinue_Execute);
			// 
			// PrintSubmittal
			// 
			this.PrintSubmittal.AcceptButtonCaption = null;
			this.PrintSubmittal.CancelButtonCaption = null;
			this.PrintSubmittal.Caption = "Submittal";
			this.PrintSubmittal.Category = "PrintSubmittal";
			this.PrintSubmittal.ConfirmationMessage = null;
			this.PrintSubmittal.Id = "PrintSubmittal";
			this.PrintSubmittal.TargetObjectType = typeof(Quotations.Module.BusinessObjects.Quotations.QuotHd);
			this.PrintSubmittal.ToolTip = null;
			this.PrintSubmittal.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PrintSubmittal_CustomizePopupWindowParams);
			this.PrintSubmittal.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PrintSubmittal_Execute);
			// 
			// PrintQuotation
			// 
			this.PrintQuotation.AcceptButtonCaption = null;
			this.PrintQuotation.CancelButtonCaption = null;
			this.PrintQuotation.Caption = "Print Quotation";
			this.PrintQuotation.Category = "PrintQuotation";
			this.PrintQuotation.ConfirmationMessage = null;
			this.PrintQuotation.Id = "PrintQuotation";
			this.PrintQuotation.TargetObjectType = typeof(Quotations.Module.BusinessObjects.Quotations.QuotHd);
			this.PrintQuotation.ToolTip = null;
			this.PrintQuotation.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PrintQuotation_CustomizePopupWindowParams);
			this.PrintQuotation.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PrintQuotation_Execute);
			// 
			// QuotHd_DetailViewController
			// 
			this.Actions.Add(this.QuotContinue);
			this.Actions.Add(this.PrintSubmittal);
			this.Actions.Add(this.PrintQuotation);
			this.TargetObjectType = typeof(Quotations.Module.BusinessObjects.Quotations.QuotHd);
			this.TargetViewId = "QuotHd_DetailView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.PopupWindowShowAction QuotContinue;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction PrintSubmittal;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction PrintQuotation;
	}
}
