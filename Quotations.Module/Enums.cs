﻿using DevExpress.ExpressApp.DC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quotations.Module
{
	public class Enums
	{
		public enum QuoteStatus
		{
			Active = 1,
			Inactive = 2
		};

		public enum ProjectStatus
		{
			Active = 1,
			Inactive = 2
		};

		public enum ItemStatus
		{
			Active = 1,
			Discontinued = 2
		};

		public enum CustomerStatus
		{
			Active = 1,
			Inactive = 2
		};

		public enum ReportType
		{
			Quotation = 0,
			Submittal = 1
		}

		public enum FormType
		{
			Blank = 0,
			PrePrinted = 1
		}

		public enum QuotationPrintOptions
		{
			None = 0,
			[XafDisplayName("Cost & Labor Time")]
			CostLabor = 1,
			ItemPrice = 2
		}

		public enum SubmittalPrintOptions
		{
			None = 0, 
			Quantity = 1, 
			LaborTime = 2,
			[XafDisplayName("Quantity & Labor Time")]
			QtyLabor = 3
		}
	}
}
