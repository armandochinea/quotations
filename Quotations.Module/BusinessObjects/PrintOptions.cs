﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace Quotations.Module.BusinessObjects.Quotations
{
	[DomainComponent]
	[DefaultClassOptions, NavigationItem(false)]
	//[ImageName("BO_Unknown")]
	//[DefaultProperty("SampleProperty")]
	//[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
	// Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
	public class PrintOptions : IXafEntityObject, IObjectSpaceLink, INotifyPropertyChanged
	{
		private IObjectSpace objectSpace;
		private void OnPropertyChanged(String propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		public PrintOptions()
		{
		}
		// Add this property as the key member in the CustomizeTypesInfo event
		[Browsable(false)]  // Hide the entity identifier from UI.
		public int ID { get; set; }

		private decimal fQuotNo;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public decimal QuotNo
		{
			get { return fQuotNo; }
			set
			{
				if (fQuotNo != value)
				{
					fQuotNo = value;
					OnPropertyChanged("QuotNo");
				}
			}
		}

		private Enums.ReportType fReportType;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public Enums.ReportType ReportType
		{
			get { return fReportType; }
			set
			{
				if (fReportType != value)
				{
					fReportType = value;
					OnPropertyChanged("ReportType");
				}
			}
		}

		private Enums.FormType fFormType;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public Enums.FormType FormType
		{
			get { return fFormType; }
			set
			{
				if (fFormType != value)
				{
					fFormType = value;
					OnPropertyChanged("FormType");
				}
			}
		}

		private Enums.QuotationPrintOptions fQuotationPrintOptions;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public Enums.QuotationPrintOptions QuotationPrintOptions
		{
			get { return fQuotationPrintOptions; }
			set
			{
				if (fQuotationPrintOptions != value)
				{
					fQuotationPrintOptions = value;
					OnPropertyChanged("QuotationPrintOptions");
				}
			}
		}

		private Enums.SubmittalPrintOptions fSubmittalPrintOptions;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public Enums.SubmittalPrintOptions SubmittalPrintOptions
		{
			get { return fSubmittalPrintOptions; }
			set
			{
				if (fSubmittalPrintOptions != value)
				{
					fSubmittalPrintOptions = value;
					OnPropertyChanged("SubmittalPrintOptions");
				}
			}
		}

		private QuotPrt fQuotPrt;
		//[XafDisplayName("My display name"), ToolTip("My hint message")]
		//[ModelDefault("EditMask", "(000)-00"), VisibleInListView(false)]
		//[RuleRequiredField(DefaultContexts.Save)]
		public QuotPrt QuotPrt
		{
			get { return fQuotPrt; }
			set
			{
				if (fQuotPrt != value)
				{
					fQuotPrt = value;
					OnPropertyChanged("QuotPrt");
				}
			}
		}
		//[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
		//public void ActionMethod() {
		//    // Trigger custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
		//    this.SampleProperty = "Paid";
		//}

		#region IXafEntityObject members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIXafEntityObjecttopic.aspx)
		void IXafEntityObject.OnCreated()
		{
			// Place the entity initialization code here.
			// You can initialize reference properties using Object Space methods; e.g.:
			// this.Address = objectSpace.CreateObject<Address>();
		}
		void IXafEntityObject.OnLoaded()
		{
			// Place the code that is executed each time the entity is loaded here.
		}
		void IXafEntityObject.OnSaving()
		{
			// Place the code that is executed each time the entity is saved here.
		}
		#endregion

		#region IObjectSpaceLink members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIObjectSpaceLinktopic.aspx)
		// Use the Object Space to access other entities from IXafEntityObject methods (see https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113707.aspx).
		IObjectSpace IObjectSpaceLink.ObjectSpace
		{
			get { return objectSpace; }
			set { objectSpace = value; }
		}
		#endregion

		#region INotifyPropertyChanged members (see http://msdn.microsoft.com/en-us/library/system.componentmodel.inotifypropertychanged(v=vs.110).aspx)
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion
	}
}