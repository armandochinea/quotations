﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class UserAuthority : XPLiteObject
	{
		public UserAuthority(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		#region Properties
		//SecuritySystemUser fOid;
		//[Key(true)]
		//public SecuritySystemUser Oid
		//{
		//	get { return fOid; }
		//	set { SetPropertyValue<SecuritySystemUser>("Oid", ref fOid, value); }
		//}
		bool fCompany;
		public bool Company
		{
			get { return fCompany; }
			set { SetPropertyValue<bool>("Company", ref fCompany, value); }
		}
		bool fUserAccess;
		public bool UserAccess
		{
			get { return fUserAccess; }
			set { SetPropertyValue<bool>("UserAccess", ref fUserAccess, value); }
		}
		string fFullName;
		[Size(50)]
		public string FullName
		{
			get { return fFullName; }
			set { SetPropertyValue<string>("FullName", ref fFullName, value); }
		}
		string fUserPassword;
		[Size(10)]
		public string UserPassword
		{
			get { return fUserPassword; }
			set { SetPropertyValue<string>("UserPassword", ref fUserPassword, value); }
		}
		DateTime fDateAdded;
		public DateTime DateAdded
		{
			get { return fDateAdded; }
			set { SetPropertyValue<DateTime>("DateAdded", ref fDateAdded, value); }
		}
		DateTime fDateModified;
		public DateTime DateModified
		{
			get { return fDateModified; }
			set { SetPropertyValue<DateTime>("DateModified", ref fDateModified, value); }
		}
		bool fQuotation;
		public bool Quotation
		{
			get { return fQuotation; }
			set { SetPropertyValue<bool>("Quotation", ref fQuotation, value); }
		}
		bool fNewQuot;
		public bool NewQuot
		{
			get { return fNewQuot; }
			set { SetPropertyValue<bool>("NewQuot", ref fNewQuot, value); }
		}
		bool fModQuot;
		public bool ModQuot
		{
			get { return fModQuot; }
			set { SetPropertyValue<bool>("ModQuot", ref fModQuot, value); }
		}
		bool fLstQuot;
		public bool LstQuot
		{
			get { return fLstQuot; }
			set { SetPropertyValue<bool>("LstQuot", ref fLstQuot, value); }
		}
		bool fQuotFLst;
		public bool QuotFLst
		{
			get { return fQuotFLst; }
			set { SetPropertyValue<bool>("QuotFLst", ref fQuotFLst, value); }
		}
		bool fQuotInq;
		public bool QuotInq
		{
			get { return fQuotInq; }
			set { SetPropertyValue<bool>("QuotInq", ref fQuotInq, value); }
		}
		bool fInvoice;
		public bool Invoice
		{
			get { return fInvoice; }
			set { SetPropertyValue<bool>("Invoice", ref fInvoice, value); }
		}
		bool fNewInv;
		public bool NewInv
		{
			get { return fNewInv; }
			set { SetPropertyValue<bool>("NewInv", ref fNewInv, value); }
		}
		bool fAddFQuot;
		public bool AddFQuot
		{
			get { return fAddFQuot; }
			set { SetPropertyValue<bool>("AddFQuot", ref fAddFQuot, value); }
		}
		bool fModInv;
		public bool ModInv
		{
			get { return fModInv; }
			set { SetPropertyValue<bool>("ModInv", ref fModInv, value); }
		}
		bool fInvFLst;
		public bool InvFLst
		{
			get { return fInvFLst; }
			set { SetPropertyValue<bool>("InvFLst", ref fInvFLst, value); }
		}
		bool fPrtInvCust;
		public bool PrtInvCust
		{
			get { return fPrtInvCust; }
			set { SetPropertyValue<bool>("PrtInvCust", ref fPrtInvCust, value); }
		}
		bool fPrtInvBTCN;
		public bool PrtInvBTCN
		{
			get { return fPrtInvBTCN; }
			set { SetPropertyValue<bool>("PrtInvBTCN", ref fPrtInvBTCN, value); }
		}
		bool fInvInq;
		public bool InvInq
		{
			get { return fInvInq; }
			set { SetPropertyValue<bool>("InvInq", ref fInvInq, value); }
		}
		bool fPurchase;
		public bool Purchase
		{
			get { return fPurchase; }
			set { SetPropertyValue<bool>("Purchase", ref fPurchase, value); }
		}
		bool fNewPurch;
		public bool NewPurch
		{
			get { return fNewPurch; }
			set { SetPropertyValue<bool>("NewPurch", ref fNewPurch, value); }
		}
		bool fModPurch;
		public bool ModPurch
		{
			get { return fModPurch; }
			set { SetPropertyValue<bool>("ModPurch", ref fModPurch, value); }
		}
		bool fLstPurch;
		public bool LstPurch
		{
			get { return fLstPurch; }
			set { SetPropertyValue<bool>("LstPurch", ref fLstPurch, value); }
		}
		bool fPurchFLst;
		public bool PurchFLst
		{
			get { return fPurchFLst; }
			set { SetPropertyValue<bool>("PurchFLst", ref fPurchFLst, value); }
		}
		bool fPurchInq;
		public bool PurchInq
		{
			get { return fPurchInq; }
			set { SetPropertyValue<bool>("PurchInq", ref fPurchInq, value); }
		}
		bool fProject;
		public bool Project
		{
			get { return fProject; }
			set { SetPropertyValue<bool>("Project", ref fProject, value); }
		}
		bool fProjMaint;
		public bool ProjMaint
		{
			get { return fProjMaint; }
			set { SetPropertyValue<bool>("ProjMaint", ref fProjMaint, value); }
		}
		bool fProjRpt;
		public bool ProjRpt
		{
			get { return fProjRpt; }
			set { SetPropertyValue<bool>("ProjRpt", ref fProjRpt, value); }
		}
		bool fProjInq;
		public bool ProjInq
		{
			get { return fProjInq; }
			set { SetPropertyValue<bool>("ProjInq", ref fProjInq, value); }
		}
		bool fMaintenance;
		public bool Maintenance
		{
			get { return fMaintenance; }
			set { SetPropertyValue<bool>("Maintenance", ref fMaintenance, value); }
		}
		bool fCustomer;
		public bool Customer
		{
			get { return fCustomer; }
			set { SetPropertyValue<bool>("Customer", ref fCustomer, value); }
		}
		bool fManufacturer;
		public bool Manufacturer
		{
			get { return fManufacturer; }
			set { SetPropertyValue<bool>("Manufacturer", ref fManufacturer, value); }
		}
		bool fItem;
		public bool Item
		{
			get { return fItem; }
			set { SetPropertyValue<bool>("Item", ref fItem, value); }
		}
		bool fInquiry;
		public bool Inquiry
		{
			get { return fInquiry; }
			set { SetPropertyValue<bool>("Inquiry", ref fInquiry, value); }
		}
		bool fCustInq;
		public bool CustInq
		{
			get { return fCustInq; }
			set { SetPropertyValue<bool>("CustInq", ref fCustInq, value); }
		}
		bool fManufacInq;
		public bool ManufacInq
		{
			get { return fManufacInq; }
			set { SetPropertyValue<bool>("ManufacInq", ref fManufacInq, value); }
		}
		bool fItemInq;
		public bool ItemInq
		{
			get { return fItemInq; }
			set { SetPropertyValue<bool>("ItemInq", ref fItemInq, value); }
		}
		bool fListing;
		public bool Listing
		{
			get { return fListing; }
			set { SetPropertyValue<bool>("Listing", ref fListing, value); }
		}
		bool fCustLst;
		public bool CustLst
		{
			get { return fCustLst; }
			set { SetPropertyValue<bool>("CustLst", ref fCustLst, value); }
		}
		bool fManufacLst;
		public bool ManufacLst
		{
			get { return fManufacLst; }
			set { SetPropertyValue<bool>("ManufacLst", ref fManufacLst, value); }
		}
		bool fItemLst;
		public bool ItemLst
		{
			get { return fItemLst; }
			set { SetPropertyValue<bool>("ItemLst", ref fItemLst, value); }
		}
		bool fProjectLst;
		public bool ProjectLst
		{
			get { return fProjectLst; }
			set { SetPropertyValue<bool>("ProjectLst", ref fProjectLst, value); }
		}
		bool fCustLbls;
		public bool CustLbls
		{
			get { return fCustLbls; }
			set { SetPropertyValue<bool>("CustLbls", ref fCustLbls, value); }
		}
		bool fSetUp;
		public bool SetUp
		{
			get { return fSetUp; }
			set { SetPropertyValue<bool>("SetUp", ref fSetUp, value); }
		}
		bool fpROJLSTCST;
		public bool pROJLSTCST
		{
			get { return fpROJLSTCST; }
			set { SetPropertyValue<bool>("pROJLSTCST", ref fpROJLSTCST, value); }
		}
		bool fQCOST;
		public bool QCOST
		{
			get { return fQCOST; }
			set { SetPropertyValue<bool>("QCOST", ref fQCOST, value); }
		}
		bool fQLABOR;
		public bool QLABOR
		{
			get { return fQLABOR; }
			set { SetPropertyValue<bool>("QLABOR", ref fQLABOR, value); }
		}
		bool fQIPRICE;
		public bool QIPRICE
		{
			get { return fQIPRICE; }
			set { SetPropertyValue<bool>("QIPRICE", ref fQIPRICE, value); }
		}
		bool fService;
		public bool Service
		{
			get { return fService; }
			set { SetPropertyValue<bool>("Service", ref fService, value); }
		}
		bool fProjCost;
		public bool ProjCost
		{
			get { return fProjCost; }
			set { SetPropertyValue<bool>("ProjCost", ref fProjCost, value); }
		}
		bool fCustServ;
		public bool CustServ
		{
			get { return fCustServ; }
			set { SetPropertyValue<bool>("CustServ", ref fCustServ, value); }
		}
		bool fSquot;
		public bool Squot
		{
			get { return fSquot; }
			set { SetPropertyValue<bool>("Squot", ref fSquot, value); }
		}
		bool fSInv;
		public bool SInv
		{
			get { return fSInv; }
			set { SetPropertyValue<bool>("SInv", ref fSInv, value); }
		}
		bool fSjob;
		public bool Sjob
		{
			get { return fSjob; }
			set { SetPropertyValue<bool>("Sjob", ref fSjob, value); }
		}
		bool fSPur;
		public bool SPur
		{
			get { return fSPur; }
			set { SetPropertyValue<bool>("SPur", ref fSPur, value); }
		}
		bool fStime;
		public bool Stime
		{
			get { return fStime; }
			set { SetPropertyValue<bool>("Stime", ref fStime, value); }
		}
		string fSServ;
		[Size(50)]
		public string SServ
		{
			get { return fSServ; }
			set { SetPropertyValue<string>("SServ", ref fSServ, value); }
		}
		bool fCSLT;
		public bool CSLT
		{
			get { return fCSLT; }
			set { SetPropertyValue<bool>("CSLT", ref fCSLT, value); }
		}
		bool fBtcSec;
		public bool BtcSec
		{
			get { return fBtcSec; }
			set { SetPropertyValue<bool>("BtcSec", ref fBtcSec, value); }
		}
		bool fViewSec;
		public bool ViewSec
		{
			get { return fViewSec; }
			set { SetPropertyValue<bool>("ViewSec", ref fViewSec, value); }
		}
		#endregion
	}
}