﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;

namespace Quotations.Module.BusinessObjects.Quotations
{
	[DefaultProperty("ID")]
	public partial class Item
	{
		public Item(Session session) : base(session) { }
		public override void AfterConstruction() {
			base.AfterConstruction();

			ActiveCode = Enums.ItemStatus.Active;
			Model = string.Empty;
			Description = string.Empty;
			Manufacturer = string.Empty;
			Cost = 0;
			Price1 = 0;
			Price2 = 0;
			Price3 = 0;
			LaborTime = 0;
			HourRate = 0;
			Company company = (Company)Session.FindObject(typeof(Company), CriteriaOperator.Parse("ID == 1"));
			if (company != null)
			{
				Percent1 = company.Percent1;
				Percent2 = company.Percent2;
				Percent3 = company.Percent3;
			}
		}
		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Cost")
			{
				Price1 = (decimal)newValue * ((decimal)Percent1 / 100 + 1);
				Price2 = (decimal)newValue * ((decimal)Percent2 / 100 + 1);
				Price3 = (decimal)newValue * ((decimal)Percent3 / 100 + 1);
			}

			if (propertyName == "Model")
			{
				this.Model = newValue.ToString().ToUpper();
			}

			if (propertyName == "Description")
			{
				this.Description = newValue.ToString().ToUpper();
			}

			if (propertyName == "Percent1")
			{
				decimal newPerc = (decimal)(Single)newValue / 100 + 1;
				Price1 = Cost * newPerc;
			}

			if (propertyName == "Percent2")
			{
				decimal newPerc = (decimal)(Single)newValue / 100 + 1;
				Price2 = Cost * newPerc;
			}

			if (propertyName == "Percent3")
			{
				decimal newPerc = (decimal)(Single)newValue / 100 + 1;
				Price3 = Cost * newPerc;
			}
		}
		protected override void OnSaving()
		{
			base.OnSaving();

			// Calculate new ID only if it's a new Item (ID = 0)
			if (ID == 0)
			{
				ID = (Decimal)Session.ExecuteScalar("SELECT MAX(ItemNo) + 1 FROM Item;");
			}
		}

		#region Properties
		Enums.ItemStatus fActiveCode;
		[Persistent("ItemActCode")]
		public Enums.ItemStatus ActiveCode
		{
			get { return fActiveCode; }
			set { SetPropertyValue<Enums.ItemStatus>("ActiveCode", ref fActiveCode, value); }
		}
		#endregion
	}

}
