﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace Quotations.Module.BusinessObjects.Quotations
{
    public partial class Customers
	{
		public Customers(Session session) : base(session) { }
		public override void AfterConstruction() {
			base.AfterConstruction();

			ActiveCode = Enums.CustomerStatus.Active;
		}
		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Name")
			{
				this.Name = newValue.ToString().ToUpper();
			}

			if (propertyName == "Address1")
			{
				this.Address1 = newValue.ToString().ToUpper();
			}

			if (propertyName == "Address2")
			{
				this.Address2 = newValue.ToString().ToUpper();
			}

			if (propertyName == "City")
			{
				this.City = newValue.ToString().ToUpper();
			}

			if (propertyName == "State")
			{
				this.State = newValue.ToString().ToUpper();
			}

			if (propertyName == "Other")
			{
				this.Other = newValue.ToString().ToUpper();
			}

			if (propertyName == "ContactName")
			{
				this.ContactName = newValue.ToString().ToUpper();
			}
		}
		protected override void OnSaving()
		{
			base.OnSaving();

			if (ID == 0)
			{
				ID = (Decimal)Session.ExecuteScalar("SELECT MAX(CustNo) + 1 FROM Customers;");
			}
		}

		#region Properties
		Enums.CustomerStatus fActiveCode;
		[Persistent("CustActCode")]
		public Enums.CustomerStatus ActiveCode
		{
			get { return fActiveCode; }
			set { SetPropertyValue<Enums.CustomerStatus>("ActiveCode", ref fActiveCode, value); }
		}
        #endregion
    }

}
