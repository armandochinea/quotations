﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class Project : XPLiteObject
	{
		decimal fID;
		[Key]
		[Persistent(@"ProjectNo")]
		public decimal ID
		{
			get { return fID; }
			set { SetPropertyValue<decimal>("ID", ref fID, value); }
		}
		string fName;
		[Size(30)]
		[Persistent(@"ProjName")]
		public string Name
		{
			get { return fName; }
			set { SetPropertyValue<string>("Name", ref fName, value); }
		}
		string fComments;
		[Size(30)]
		[Persistent(@"ProjCommt")]
		public string Comments
		{
			get { return fComments; }
			set { SetPropertyValue<string>("Comments", ref fComments, value); }
		}
		Customers fCustomer;
		[Persistent(@"ProjCustNo")]
		public Customers Customer
		{
			get { return fCustomer; }
			set { SetPropertyValue<Customers>("Customer", ref fCustomer, value); }
		}
		string fSupervisor;
		[Size(50)]
		public string Supervisor
		{
			get { return fSupervisor; }
			set { SetPropertyValue<string>("Supervisor", ref fSupervisor, value); }
		}
		string fAddress1;
		[Size(50)]
		[Persistent(@"Add1")]
		public string Address1
		{
			get { return fAddress1; }
			set { SetPropertyValue<string>("Address1", ref fAddress1, value); }
		}
		string fAddress2;
		[Size(50)]
		[Persistent(@"Add2")]
		public string Address2
		{
			get { return fAddress2; }
			set { SetPropertyValue<string>("Address2", ref fAddress2, value); }
		}
		string fCity;
		[Size(30)]
		public string City
		{
			get { return fCity; }
			set { SetPropertyValue<string>("City", ref fCity, value); }
		}
		string fState;
		[Size(2)]
		public string State
		{
			get { return fState; }
			set { SetPropertyValue<string>("State", ref fState, value); }
		}
		string fZipCode;
		[Size(11)]
		[Persistent(@"Zip")]
		public string ZipCode
		{
			get { return fZipCode; }
			set { SetPropertyValue<string>("ZipCode", ref fZipCode, value); }
		}
		string fPhone;
		[Size(15)]
		[Persistent(@"Tel")]
		public string Phone
		{
			get { return fPhone; }
			set { SetPropertyValue<string>("Phone", ref fPhone, value); }
		}
		string fFax;
		[Size(15)]
		public string Fax
		{
			get { return fFax; }
			set { SetPropertyValue<string>("Fax", ref fFax, value); }
		}
		[DevExpress.Persistent.Base.ImmediatePostData]
		[Association(@"QuotHdReferencesProject")]
		public XPCollection<QuotHd> Quotations { get { return GetCollection<QuotHd>("Quotations"); } }
	}

}
