﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class Company
	{
		public Company(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		#region Properties
		private string fEmail;
		[Persistent("Email"), VisibleInListView(false), VisibleInLookupListView(false)]
		public string Email
		{
			get { return fEmail; }
			set { SetPropertyValue<string>("Email", ref fEmail, value); }
		}

		[Persistent("ReportLogo"), Delayed, MemberDesignTimeVisibility(true), ImageEditor, VisibleInListView(false)]
		public byte[] ReportLogo
		{
			get { return GetDelayedPropertyValue<byte[]>("ReportLogo"); }
			set { SetDelayedPropertyValue<byte[]>("ReportLogo", value); }
		}

		[NonPersistent, VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string FullPostalAddress
		{
			get { return string.Format("{0}{1} {2}, {3} {4}", PostalAddress1, string.IsNullOrEmpty(PostalAddress2) ? "" : " " + PostalAddress2, PostalCity, PostalState, PostalZipCode); }
		}

		[NonPersistent, VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(true)]
		public string CompanyInfo
		{
			get { return string.Format("{0} {1} | {2} | T.{3} | F.{4}", Name ?? "Business Tele-Communications, Inc.", FullPostalAddress ?? "PO Box 16635 San Juan, PR 00908-6635", Email ?? "btc@btc84.com", Phone1 ?? "(787) 764-2019", Fax1 ?? "(787) 764-2923"); }
		}
		#endregion
	}

}
