﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Persistent.Base;

namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class Project
	{
		public Project(Session session) : base(session) { }
		public override void AfterConstruction() {
			base.AfterConstruction();
			
			Name = string.Empty;
			Comments = string.Empty;
			Status = Enums.ProjectStatus.Active;
			Supervisor = string.Empty;
			Address1 = string.Empty;
			Address2 = string.Empty; ;
			City = string.Empty;
			State = string.Empty;
			ZipCode = string.Empty;
			Phone = string.Empty;
			Fax = string.Empty;
		}
		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Name")
			{
				this.Name = newValue.ToString().ToUpper();
			}

			if (propertyName == "Comments")
			{
				this.Comments = newValue.ToString().ToUpper();
			}

			if (propertyName == "Supervisor")
			{
				this.Supervisor = newValue.ToString().ToUpper();
			}

			if (propertyName == "Address1")
			{
				this.Address1 = newValue.ToString().ToUpper();
			}

			if (propertyName == "Address2")
			{
				this.Address2 = newValue.ToString().ToUpper();
			}

			if (propertyName == "City")
			{
				this.City = newValue.ToString().ToUpper();
			}

			if (propertyName == "State")
			{
				this.State = newValue.ToString().ToUpper();
			}
		}
		protected override void OnSaving()
		{
			base.OnSaving();

			if (ID == 0)
			{
				ID = (Decimal)Session.ExecuteScalar("SELECT MAX(ProjectNo) + 1 FROM Project;");
			}
		}

		#region Properties
		Enums.ProjectStatus fStatus;
		[Persistent("status")]
		public Enums.ProjectStatus Status
		{
			get { return fStatus; }
			set { SetPropertyValue<Enums.ProjectStatus>("Status", ref fStatus, value); }
		}
		//Decimal fCost;
		[Persistent(@"ProjCost")]
		public decimal Cost
		{
			get { return Quotations.Sum(d => d.ExtPrice); }
			//set { SetPropertyValue<Decimal>("Cost", ref fCost, value); }
		}
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), NonPersistent]
		public string strCost => Cost.ToString("$#,##0.00");
		#endregion
	}
}
