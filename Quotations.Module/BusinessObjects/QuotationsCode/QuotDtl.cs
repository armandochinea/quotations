﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace Quotations.Module.BusinessObjects.Quotations
{
    [OptimisticLocking(true)]
    public partial class QuotDtl
	{
		public QuotDtl(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Qty")
			{
				decimal mQty = (decimal)newValue;
				if (mQty <= 0)
				{
					mQty = 0;
					this.Qty = mQty;
				}
				Company company = (Company)Session.FindObject(typeof(Company), CriteriaOperator.Parse("ID == 1"));
				if (company != null)
				{
					if (mQty >= company.PriceStart1 && mQty <= company.PriceEnd1)
					{
						this.Price = this.Item.Price1;
					}
					else if (mQty >= company.PriceStart2 && mQty <= company.PriceEnd2)
					{
						this.Price = this.Item.Price2;
					}
					else if (mQty >= company.PriceStart3 && mQty <= company.PriceEnd3)
					{
						this.Price = this.Item.Price3;
					}
					else
					{
						this.Price = 0;
					}
				}
			}
		}

		#region Properties
		decimal fPrice;
		public decimal Price
		{
			get { return fPrice; }
			set { SetPropertyValue<decimal>("Price", ref fPrice, value); }
		}

		[Persistent("ExtPrice"), ImmediatePostData]
		public decimal ExtPrice { get { return this.Qty * this.Price; } }
		#endregion
	}

}
