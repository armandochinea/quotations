﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class QuotPrt
	{
		public QuotPrt(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Accept")
			{
				this.Accept = newValue.ToString().ToUpper();
			}

			if (propertyName == "By")
			{
				this.By = newValue.ToString().ToUpper();
			}
		}
	}

}
