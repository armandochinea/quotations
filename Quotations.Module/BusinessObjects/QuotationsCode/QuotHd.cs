﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.DC;

namespace Quotations.Module.BusinessObjects.Quotations
{
	[VisibleInReports(true), OptimisticLocking(true)]
	public partial class QuotHd
	{
		public QuotHd(Session session) : base(session) { }
		public override void AfterConstruction() {
			base.AfterConstruction();

			ID = (Decimal)Session.ExecuteScalar("SELECT MAX(QuotNo) + 1 FROM QuotHd;");
			ProjectName = string.Empty;
			ActiveCode = Enums.QuoteStatus.Active;
			Date = DateTime.Today;
			SalePerson = Session.FindObject<BtcUser>(CriteriaOperator.Parse("UserName == ?", SecuritySystem.CurrentUserName)).FullName;
			DeliveryDate = "4 TO 6 WEEKS";
			Terms = "SEE NOTES";
			Shipped = "INSTALLED";
			FOB = "SAN JUAN";
			AdNotes = "1. ONE YEAR WARRANTY. 2. ELECTRICAL CONTRACTOR WILL INSTALL THE CONDUIT, GREEN FIELD AND FISH WIRING. 3. TERMS 50% WITH ORDER AND 50% WITH COMPLETE INSTALLATION. 4. IVU TAX ARE NOT INCLUDED.";
			System = string.Empty;
			DiscountPercent = 0;
			OtherCharges = 0;
			TaxPercent = (decimal)11.5;
			SalesCommissionPercent = 5;
			IsLegacy = false;
			Company = Session.GetObjectByKey<Company>((decimal)1);
			//QuotPrt = Session.GetObjectByKey<QuotPrt>((decimal)this.ID);
			var quotPrt = new QuotPrt(Session);
			quotPrt.QuotNo = this;
			quotPrt.Accept = string.Empty;
			quotPrt.By = SalePerson;
			quotPrt.DatePrt = DateTime.MinValue;
			QuotPrt = quotPrt;
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "TaxPercent")
			{
				this.Tax = Math.Round((SubTotal + Discount) * (Math.Abs((decimal)newValue) / (decimal)100.0), 2);
			}

			if (propertyName == "SalesCommissionPercent")
			{
				// Other Charges are subtracted and not considered in Sales Commissions' calculations.
				this.SalesCommission = Math.Round(((SubTotal - OtherCharges) + Discount) * ((decimal)newValue / (decimal)100.0), 2);
			}

			if (propertyName == "DiscountPercent")
			{
				this.Discount = Math.Round((SubTotal * (Math.Abs((decimal)newValue) / (decimal)100.0)) * -1, 2);

				// This is done to recalculate Tax and Comission base on new Discount applied.
				if (!IsLegacy)
				{
					var tempTaxPerc = this.TaxPercent;
					this.TaxPercent = 0;
					this.TaxPercent = tempTaxPerc;

					var tempComission = this.SalesCommissionPercent;
					this.SalesCommissionPercent = 0;
					this.SalesCommissionPercent = tempComission;
				}
			}

			if (propertyName == "OtherCharges")
			{
				// This is done to recalculate Tax and Comission base on new Discount applied.
				var tempTaxPerc = this.TaxPercent;
				this.TaxPercent = 0;
				this.TaxPercent = tempTaxPerc;
			}

			if (propertyName == "ProjectName")
			{
				this.ProjectName = newValue.ToString().ToUpper();
			}

			if (propertyName == "SalePerson")
			{
				this.SalePerson = newValue.ToString().ToUpper();
			}

			if (propertyName == "DeliveryDate")
			{
				this.DeliveryDate = newValue.ToString().ToUpper();
			}

			if (propertyName == "Terms")
			{
				this.Terms = newValue.ToString().ToUpper();
			}

			if (propertyName == "Shipped")
			{
				this.Shipped = newValue.ToString().ToUpper();
			}

			if (propertyName == "FOB")
			{
				this.FOB = newValue.ToString().ToUpper();
			}

			if (propertyName == "AdNotes")
			{
				this.AdNotes = newValue.ToString().ToUpper();
			}

			if (propertyName == "System")
			{
				this.System = newValue.ToString().ToUpper();
			}
		}

		#region Properties
		Enums.QuoteStatus fActiveCode;
		[Persistent("ActCode")]
		public Enums.QuoteStatus ActiveCode
		{
			get { return fActiveCode; }
			set { SetPropertyValue<Enums.QuoteStatus>("ActiveCode", ref fActiveCode, value); }
		}

		[Persistent("Labor"), ImmediatePostData]
		public decimal Labor
		{
			get { return this.QuotDtls.Sum(d => (decimal)d.Item.LaborTime * (decimal)d.Item.HourRate * d.Qty); }
		}

		private bool fIsLegacy;
		[Persistent("IsLegacy"), VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool IsLegacy
		{
			get { return fIsLegacy; }
			set { SetPropertyValue<bool>("IsLegacy", ref fIsLegacy, value); }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strLabor => Labor.ToString("$ #,##0.00");

		[NonPersistent, VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public decimal SubTotal => ItemsTotal + Labor + OtherCharges;

		[Persistent("ExtPrice"), ImmediatePostData]
		public decimal ExtPrice
		{
			get { return IsLegacy ? TotalOtherCharges + ItemsTotal : SubTotal + Discount + Tax + SalesCommission; }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), ImmediatePostData]
		public decimal TotalOtherCharges
		{
			get { return IsLegacy ? Freight + Labor + Cable + Tax + Other : Discount + Labor + OtherCharges + Tax + SalesCommission; }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), ImmediatePostData]
		public decimal ItemsTotal
		{
			get
			{
				if (!Session.IsObjectsSaving)
				{
					return QuotDtls.Sum(d => d.ExtPrice);
				}
				return 0;
			}
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strExtPrice => ExtPrice.ToString("$ #,##0.00");

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strTotalOtherCharges => TotalOtherCharges.ToString("$ #,##0.00");

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strItemsTotal => ItemsTotal.ToString("$ #,##0.00");

		private decimal fDiscountPercent;
		[Persistent("DiscountPercent"), ImmediatePostData, VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal DiscountPercent
		{
			get { return fDiscountPercent/* / (decimal)100.0*/; }
			set { SetPropertyValue<decimal>("DiscountPercent", ref fDiscountPercent, value); }
		}

		private decimal fDiscount;
		[Persistent("Discount"), VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal Discount
		{
			get { return fDiscount; }
			set { SetPropertyValue<decimal>("Discount", ref fDiscount, value); }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strDiscount => Discount.ToString("$ #,##0.00");

		private decimal fTaxPercent;
		[Persistent("TaxPercent"), ImmediatePostData, VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal TaxPercent
		{
			get { return fTaxPercent/* / (decimal)100.0*/; }
			set { SetPropertyValue<decimal>("TaxPercent", ref fTaxPercent, value); }
		}

		private decimal fTax;
		[Persistent("Tax"), VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal Tax
		{
			get { return fTax; }
			set { SetPropertyValue<decimal>("Tax", ref fTax, value); }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strTax => Tax.ToString("$ #,##0.00");

		private decimal fSalesCommissionPercent;
		[Persistent("SalesCommissionPercent"), ImmediatePostData, VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal SalesCommissionPercent
		{
			get { return fSalesCommissionPercent/* / (decimal)100.0*/; }
			set { SetPropertyValue<decimal>("SalesCommissionPercent", ref fSalesCommissionPercent, value); }
		}

		private decimal fSalesCommission;
		[Persistent("SalesCommission"), VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal SalesCommission
		{
			get { return fSalesCommission; }
			set { SetPropertyValue<decimal>("SalesCommission", ref fSalesCommission, value); }
		}

		[NonPersistent, VisibleInListView(false), VisibleInLookupListView(false), VisibleInReports(false)]
		public string strSalesCommission => SalesCommission.ToString("$ #,##0.00");

		string fAdNotes;
		[Size(200)]
		public string AdNotes
		{
			get { return !string.IsNullOrEmpty(fAdNotes) ? fAdNotes : ""; }
			set { SetPropertyValue<string>("AdNotes", ref fAdNotes, value); }
		}

		Company fCompany;
		[Persistent("Company"), VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Company Company
		{
			get { return fCompany; }
			set { SetPropertyValue<Company>("Company", ref fCompany, value); }
		}

		QuotPrt fQuotPrt;
		[Persistent("QuotPrt"), VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public QuotPrt QuotPrt
		{
			get { return fQuotPrt; }
			set { SetPropertyValue<QuotPrt>("QuotPrt", ref fQuotPrt, value); }
		}

		#endregion
	}
}
