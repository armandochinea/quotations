﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace Quotations.Module.BusinessObjects.Quotations
{

	public partial class ProjQuot : XPLiteObject
	{
		public struct CompoundKey1Struct
		{
			[Persistent("ProjectNo")]
			public decimal ProjectNo { get; set; }
			[Persistent("QuotationNo")]
			public decimal QuotationNo { get; set; }
			[Persistent("CustNo")]
			public decimal CustNo { get; set; }
		}
		[Key, Persistent]
		public CompoundKey1Struct CompoundKey1;
	}

}
