﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using Quotations.Module.BusinessObjects.Quotations;

namespace Quotations.Module.BusinessObjects.Quotations
{
	public partial class ItemSearch : XPLiteObject
	{
		public ItemSearch(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		Guid fID;
		[Key]
		public Guid ID
		{
			get { return fID; }
			set { SetPropertyValue<Guid>("ID", ref fID, value); }
		}
		string fUser;
		public string User
		{
			get { return fUser; }
			set { SetPropertyValue<string>("User", ref fUser, value); }
		}
		Item fItem;
		public Item Item
		{
			get { return fItem; }
			set { SetPropertyValue<Item>("Item", ref fItem, value); }
		}
		UInt32 fQty;
		public UInt32 Qty
		{
			get { return fQty; }
			set { SetPropertyValue<UInt32>("Qty", ref fQty, value); }
		}
	}
}