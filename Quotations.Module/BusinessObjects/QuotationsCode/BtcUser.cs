﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Security.Strategy;

namespace Quotations.Module.BusinessObjects.Quotations
{
	[DefaultClassOptions]
	[ImageName("BO_User"), Persistent("BtcUser")]
	public class BtcUser : SecuritySystemUser
	{ // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
		public BtcUser(Session session) : base(session)
		{
		}
		public override void AfterConstruction()
		{
			base.AfterConstruction();


		}
		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "FullName")
			{
				this.FullName = newValue.ToString().ToUpper();
			}
		}

		#region Properties
		private string fFullName;
		[Persistent("FullName")]
		public string FullName
		{
			get { return fFullName; }
			set { SetPropertyValue<string>("FullName", ref fFullName, value); }
		}

		private string fEmail;
		[Persistent("Email")]
		public string Email
		{
			get { return fEmail; }
			set { SetPropertyValue<string>("Email", ref fEmail, value); }
		}
		#endregion
	}
}